/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../../Entryfile/imagepath.jsx'
import InputMask from 'react-input-mask';
import PasswordMask from 'react-password-mask';




class Loginpage extends Component {
    constructor(props) {
    super(props);

    this.state = {
        password : ''
    };
    this.handleChange = this.handleChange.bind(this)
  }
   state = {
      email: 'demo@example.com',
      password: 'test#123'
   }

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }
      
   loginclick(){
    // this.props.history.push("/app/main/dashboard")
    localStorage.setItem("firstload","true")
   }


   render() {
     
      return (
         
         
         <div className="main-wrapper justify-content">
           <Helmet>
               <title>Configuration - PARTICIPATION HOUSE SUPPORT SERVICES</title>
               <meta name="description" content="Login page"/>          
         </Helmet>
        <div className="account-content account-content-pk">
          
          <div className="container">
            {/* Account Logo */}
            <div className="account-logo">
              <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
            </div>
            {/* /Account Logo */}
            <div className="account-box account-box-pk">
              <div className="account-wrapper">
                <h3 className="account-title">Configuration</h3>
                <p className="account-subtitle"></p>
                {/* Account Form */}
                <form action="/app/main/dashboard">
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Username <span class="text-danger">*</span></label>
                        <input className="form-control" type="text"  />
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Phone</label>
                        <InputMask className="form-control" mask="(999) 999-9999" ></InputMask>
                        <span className="form-text text-muted">(999) 999-9999</span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <div className="row">
                          <div className="col">
                            <label>Password <span class="text-danger">*</span></label>
                          </div>
                        </div>
                        <PasswordMask
                          id="password"
                          name="password"
                          maxLength='14'
                          buttonClassName="hideshowbtn"
                          inputClassName="form-control"
                          //placeholder="Enter password"
                          value={this.state.password}
                          onChange={this.handleChange('password')}
                        />
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <div className="row">
                          <div className="col">
                            <label>Confirm Password <span class="text-danger">*</span></label>
                          </div>
                        </div>
                        <PasswordMask
                          id="password"
                          name="password"
                          maxLength='14'
                          buttonClassName="hideshowbtn"
                          inputClassName="form-control"
                          //placeholder="Enter password"
                          value={this.state.password}
                          onChange={this.handleChange('password')}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #1 <span class="text-danger">*</span></label>
                        <select className="select">
                          <option>Select</option>
                          <option value="0">Where was your mother born?</option>
                                      <option value="1">What was your first car?</option>
                                      <option value="2">What was the first name of your first pet?</option>
                                      <option value="3">What was your school name?</option>
                                      <option value="4">Where were you born?</option>
                                      <option value="5">What model your first smartphone was?</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #1 <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" />
                      </div>
                    </div>
                  </div>
                  

                  <div className="row">
                    
                    <div className="col-sm-6">
                        <div className="form-group">
                          <label>Question #2 <span class="text-danger">*</span></label>
                          <select className="select">
                            <option>Select</option>
                            <option value="0">Where was your mother born?</option>
                                        <option value="1">What was your first car?</option>
                                        <option value="2">What was the first name of your first pet?</option>
                                        <option value="3">What was your school name?</option>
                                        <option value="4">Where were you born?</option>
                                        <option value="5">What model your first smartphone was?</option>
                          </select>
                        </div>
                    </div>

                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Answer #2 <span class="text-danger">*</span></label>
                        <input type="text" className="form-control" />
                      </div>
                    </div>
                    
                    
                  </div>

                  <div className="row">
                    <div className="col-sm-6">
                      <div className="form-group">
                        <label>Question #3 <span class="text-danger">*</span></label>
                        <select className="select">
                          <option>Select</option>
                          <option value="0">Where was your mother born?</option>
                          <option value="1">What was your first car?</option>
                          <option value="2">What was the first name of your first pet?</option>
                          <option value="3">What was your school name?</option>
                          <option value="4">Where were you born?</option>
                          <option value="5">What model your first smartphone was?</option>
                        </select>
                      </div>
                    </div>
                    
                    <div className="col-sm-6">
                       <div className="form-group">
                          <label>Answer #3 <span class="text-danger">*</span></label>
                          <input type="text" className="form-control" />
                        </div>
                    </div>
                  </div>
                  
                  
                  <div class="text-right">
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  <div className="account-footer">
                    
                  </div>
                </form>
                {/* /Account Form */}
              </div>
            </div>
          </div>
        </div>
      </div>
      );
   }
}

export default Loginpage;
