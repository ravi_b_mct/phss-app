/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../../Entryfile/imagepath.jsx'

class OTPscreen extends Component {

   render() {
     
      return ( 
      <div className="main-wrapper">
          <Helmet>
               <title>Security Questions - PARTICIPATION HOUSE SUPPORT SERVICES</title>
               <meta name="description" content="Login page"/>					
         </Helmet>
      <div className="account-content">
        <div className="container">
          {/* Account Logo */}
          <div className="account-logo">
            <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
          </div>
          {/* /Account Logo */}
          <div className="account-box">
            <div className="account-wrapper">
              <h3 className="account-title">Forgot Password?</h3>
              <p className="account-subtitle">Select Question and Enter Answer</p>
              {/* Account Form */}
              

             
              <form action="/app/main/dashboard">
                <h4 className="card-title">Security Question</h4>
                <div className="row">
                  <div className="col-md-12">
                    
                    <div className="form-group">
                      <label>Question #1</label>
                      <select className="select">
                        <option>Select</option>
                        <option value="0">Where was your mother born?</option>
                                    <option value="1">What was your first car?</option>
                                    <option value="2">What was the first name of your first pet?</option>
                                    <option value="3">What was your school name?</option>
                                    <option value="4">Where were you born?</option>
                                    <option value="5">What model your first smartphone was?</option>
                      </select>
                    </div>

                    <div className="form-group">
                      <label>Answer</label>
                      <input type="text" className="form-control" />
                    </div>

                    <div className="form-group">
                      <label>Question #2</label>
                      <select className="select">
                        <option>Select</option>
                        <option value="0">Where was your mother born?</option>
                                    <option value="1">What was your first car?</option>
                                    <option value="2">What was the first name of your first pet?</option>
                                    <option value="3">What was your school name?</option>
                                    <option value="4">Where were you born?</option>
                                    <option value="5">What model your first smartphone was?</option>
                      </select>
                    </div>

                    <div className="form-group">
                      <label>Answer</label>
                      <input type="text" className="form-control" />
                    </div>
                    
                  </div>
                  
                </div>
                
                <div class="col-md-12">
                  <div class="row"> 
                    <div class="col-md-6">
                      <div class="text-left">
                        <a href="/login"  class="btn btn-danger">Login</a>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="text-right">
                        <a href="/forgototp" class="btn btn-primary">Submit</a>
                      </div>
                    </div>
                    
                  </div>
                </div>
                
                
              </form>
              
              {/* /Account Form */}
            </div>
          </div>
        </div>
      </div>
    </div>
           
      );
   }
}

export default OTPscreen;
