/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import { Multiselect } from 'multiselect-react-dropdown';
import moment from 'moment';
import InputMask from 'react-input-mask';


import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import SystemHelpers from '../../../Helpers/SystemHelper';
//import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../../Entryfile/imagepath'


class ProfileView extends Component {
  constructor(props) {
    super(props);

    //this.state = { Preferred_Language_Model: [] };
    this.state = { Preferred_Language_Model: '' };

    this.state = {
        staffContactFullname : localStorage.getItem('fullName'),

        prefLanguagesList: [],
        selectedValue: [],
        errormsg :  '',
        user_role: [],
        staffContactID:this.props.staffContactID,
        all_data :this.props.all_data,
        Preferred_Name_Model :  '',
        Date_of_Birth_Model :  '',
        First_Name_Model :  '',
        Middle_Name_Model :  '',
        Last_Name_Model :  '',
        Gender_Model :  '',
        Marital_Status_Model :  '',
        gendersList:[],
        familiyStatusList:[],
        dateOfBirth_display:'',
        preferredName_display:'',
        role_profile_info_can : {}
        //Preferred_Language_Model:[]
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onRemove = this.onRemove.bind(this);
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

   // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  componentDidMount() {

    /* Role Management */
     console.log('Role Store profile_info_can');
     /*var getrole = SystemHelpers.GetRole();
     let profile_info_can = getrole.profile_info_can;
     this.setState({ role_profile_info_can: profile_info_can });
     console.log(profile_info_can);*/

     //console.log('Role Store profile_info_can 123');
     console.log(this.props.profile_info_can);
     this.setState({ role_profile_info_can: this.props.profile_info_can });
    /* Role Management */


    console.log("ProfileView");
    console.log(this.state.all_data);

    
    //
    this.setState({ preferredName_display: this.state.all_data.preferredName });

    
    this.setState({ dateOfBirth_display: this.state.all_data.dateOfBirth });
    
    
    this.setState({ firstName_display: this.state.all_data.firstName });
    this.setState({ middleName_display: this.state.all_data.middleName });
    this.setState({ lastName_display: this.state.all_data.lastName });
    this.setState({ gender_display: this.state.all_data.gender });
    this.setState({ maritalStatus_display: this.state.all_data.maritalStatus });
    this.setState({ prefLanguageDisplay_display: this.state.all_data.prefLanguageDisplay });

    // List 
    this.setState({ gendersList: this.state.all_data.gendersList });
    this.setState({ familiyStatusList: this.state.all_data.familiyStatusList });
    this.setState({ prefLanguagesList: this.state.all_data.prefLanguagesList });
    
    //

    this.setState({ Preferred_Name_Model: this.state.all_data.preferredName });

    if(this.state.all_data.dateOfBirth !='' && this.state.all_data.dateOfBirth != null){
      this.setState({ Date_of_Birth_Model: moment(this.state.all_data.dateOfBirth).format('YYYY-MM-DD') });
    }
    
    this.setState({ First_Name_Model: this.state.all_data.firstName });
    this.setState({ Middle_Name_Model: this.state.all_data.middleName });
    this.setState({ Last_Name_Model: this.state.all_data.lastName });
    this.setState({ Gender_Model: this.state.all_data.gender });
    this.setState({ Marital_Status_Model: this.state.all_data.maritalStatus });
    this.setState({ prefLanguageUpdatedValue: this.state.all_data.prefLanguageUpdatedValue });
    this.setState({ Preferred_Language_Model : this.state.all_data.prefLanguageDisplay });

    // Edit Time Show "Preferred Language"
    var all_list_pref_lang = this.state.all_data.prefLanguagesList;
    var selected_id_pref_lang = this.state.all_data.prefLanguageUpdatedValue;
    let selected_list_pref_lang = [];
    for (var i = 0; i<all_list_pref_lang.length; i++)
    {
      if(selected_id_pref_lang != null && selected_id_pref_lang.length!=0  )
      {
        for (var j = 0; j<selected_id_pref_lang.length; j++)
        {
          if(all_list_pref_lang[i].id==selected_id_pref_lang[j])
          {
            selected_list_pref_lang.push(all_list_pref_lang[i]);
          }
        }
      }
    }
    this.setState({ selectedValue: selected_list_pref_lang });
    // Edit Time Show "Preferred Language"

  }

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

  //
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ all_data: data.data});
            
            localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
            localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
            
            this.setState({ preferredName_display: data.data.preferredName });
            this.setState({ dateOfBirth_display: data.data.dateOfBirth });
            this.setState({ firstName_display: data.data.firstName });
            this.setState({ middleName_display: data.data.middleName });
            this.setState({ lastName_display: data.data.lastName });
            this.setState({ gender_display: data.data.gender });
            this.setState({ maritalStatus_display: data.data.maritalStatus });
            this.setState({ prefLanguageDisplay_display: data.data.prefLanguageDisplay });

            this.props.setPropState('Preferred_Name', data.data.preferredName);
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  //

  onSelect(selectedList, selectedItem) {
    //console.log(selectedList);
    let lang =[];

    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      //console.log(number);
      //number.toString();
      lang.push(number);
      
    }
    this.setState({ Preferred_Language_Model: lang });
    //console.log('select preferred insert');
    //console.log(lang);
  }

  onRemove(selectedList, removedItem) {
    //console.log('select preferred remove');
      //console.log(selectedList);
      let lang =[];
      for (var i = 0; i<selectedList.length; i++) {
        //console.log(selectedList[i].id)
        var number1 = selectedList[i].id;
        var number =  number1.toString();
        lang.push(number);
        
      }
      //console.log('select preferred remove length');
      //console.log(lang.length);
      if(lang.length != 0){
        this.setState({ Preferred_Language_Model: lang });
      }
      
      //console.log('select preferred remove');
      //console.log(lang);
  }

  // Profile model Update api
  UpdateUserProfileTabProfileModel_API = () => e => {  
      e.preventDefault(); 
      console.log('123456'); 
      console.log(this.state.Preferred_Language_Model);
      //return false;
      //debugger;
      let step1Errors = {};
      if (this.state["Preferred_Name_Model"] === ''  || this.state["Preferred_Name_Model"] == null || $.trim(this.state["Preferred_Name_Model"]).length==0) {
        step1Errors["Preferred_Name_Model"] = "Preferred Name is mandatory"
      }
      /*if (this.state["Date_of_Birth_Model"] === '' || this.state["Date_of_Birth_Model"] == null) {
        step1Errors["Date_of_Birth_Model"] = "Date of Birth is mandatory"
      }*/
      if (this.state["First_Name_Model"] === '' || this.state["First_Name_Model"] == null || $.trim(this.state["First_Name_Model"]).length==0) {
        step1Errors["First_Name_Model"] = "First Name is mandatory"
      }
      // if (this.state["Middle_Name_Model"] === '' || this.state["Middle_Name_Model"] == null) {
      //   step1Errors["Middle_Name_Model"] = "Middle Name is mandatory"
      // }
      if (this.state["Last_Name_Model"] === '' || this.state["Last_Name_Model"] == null || $.trim(this.state["Last_Name_Model"]).length==0) {
        step1Errors["Last_Name_Model"] = "Last Name is mandatory"
      }
      /*if (this.state["Gender_Model"] == '' || this.state["Gender_Model"] == null) {
        step1Errors["Gender_Model"] = "Gender is mandatory"
      }

      if (this.state["Marital_Status_Model"] == '' || this.state["Marital_Status_Model"] == null) {
        step1Errors["Marital_Status_Model"] = "Marital status is mandatory"
      }*/
      /*if ($('#Preferred_Language_Model').val() === 0 || $('#Preferred_Language_Model').val() === null ) {
        step1Errors["Preferred_Language_Model"] = "Preferred Language is mandatory"
      }*/

      //alert(this.state["Preferred_Language_Model"]);
      /*if (this.state["Preferred_Language_Model"] == 0 || this.state["Preferred_Language_Model"] == null || this.state["Preferred_Language_Model"] == "") {
        step1Errors["Preferred_Language_Model"] = "Preferred Language is mandatory"
      }*/
      
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();

      let User_Basic_profile = {
        preferredName: this.state["Preferred_Name_Model"],
        firstName: this.state["First_Name_Model"],
        middleName: this.state["Middle_Name_Model"],
        lastName: this.state["Last_Name_Model"],
        dateOfBirth: this.state["Date_of_Birth_Model"],
        //gender: this.state["Gender_Model"],
        //maritalStatus: this.state["Marital_Status_Model"],
        gender: "",
        maritalStatus: "",
        //preferredLanguage: this.state["Preferred_Language_Model"]
        //prefLanguageUpdatedValue: this.state["Preferred_Language_Model"]
        prefLanguageDisplay: this.state["Preferred_Language_Model"]
      };

      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["userBasicInfo"] = User_Basic_profile;
      bodyarray["userName"] = this.state.staffContactFullname;
      
      console.log(bodyarray);
      var url=process.env.API_API_URL+'UpdateUserBasicProfile';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        console.log("responseJson UpdateUserBasicProfile");
        console.log(data);
        //console.log(responseJson);
        // debugger;

        if (data.responseType === "1") {
          //this.ToastSuccess(data.responseMessge); 
          this.GetProfile();  
          this.ToastSuccess('Profile updated successfully');
          $( "#close_btn_profile" ).trigger( "click" );
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // Profile model Update api

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    //
    this.setState({ preferredName_display: this.state.all_data.preferredName });
    this.setState({ dateOfBirth_display: this.state.all_data.dateOfBirth });
    this.setState({ firstName_display: this.state.all_data.firstName });
    this.setState({ middleName_display: this.state.all_data.middleName });
    this.setState({ lastName_display: this.state.all_data.lastName });
    this.setState({ gender_display: this.state.all_data.gender });
    this.setState({ maritalStatus_display: this.state.all_data.maritalStatus });
    this.setState({ prefLanguageDisplay_display: this.state.all_data.prefLanguageDisplay });

    // List 
    this.setState({ gendersList: this.state.all_data.gendersList });
    this.setState({ familiyStatusList: this.state.all_data.familiyStatusList });
    this.setState({ prefLanguagesList: this.state.all_data.prefLanguagesList });
    
    //

    this.setState({ Preferred_Name_Model: this.state.all_data.preferredName });
    this.setState({ Date_of_Birth_Model: moment(this.state.all_data.dateOfBirth).format('YYYY-MM-DD') });
    this.setState({ First_Name_Model: this.state.all_data.firstName });
    this.setState({ Middle_Name_Model: this.state.all_data.middleName });
    this.setState({ Last_Name_Model: this.state.all_data.lastName });
    this.setState({ Gender_Model: this.state.all_data.gender });
    this.setState({ Marital_Status_Model: this.state.all_data.maritalStatus });
    this.setState({ prefLanguageUpdatedValue: this.state.all_data.prefLanguageUpdatedValue });
    this.setState({ Preferred_Language_Model : this.state.all_data.prefLanguageDisplay });

  }
  
   render() {
      return (
        
            <div className="col-md-6 d-flex">
              {(this.state.loading) ? <Loader /> : null}
              <div className="card profile-box flex-fill pk-profile-box">
                <div className="card-body">
                {this.state.role_profile_info_can.profile_info_can_update == true ? 
                  
                  <h3 className="card-title">Profile <a href="#" className="edit-icon" data-toggle="modal" data-target="#ProfileTab_profile_modal"><i className="fa fa-pencil" /></a></h3>
                  : <h3 className="card-title">Profile <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                  <ul className="personal-info">
                    <li>
                      <div className="title">Preferred Name</div>
                      {this.state.preferredName_display == '' || this.state.preferredName_display == null? <div className="text hide-font">None</div> : <div className="text">{this.state.preferredName_display}</div> }
                    </li>
                    <li>
                      <div className="title">First Name</div>
                      <div className="text">{this.state.firstName_display}</div>
                    </li>
                    <li>
                      <div className="title">Last Name</div>
                      <div className="text">{this.state.lastName_display}</div>
                    </li>
                    {/*<li>
                      <div className="title">Date of Birth</div>
                      {this.state.dateOfBirth_display == '' || this.state.dateOfBirth_display == null? <div className="text hide-font">None</div> : <div className="text">{this.state.dateOfBirth_display}</div> }
                    </li>
                    <li>
                      <div className="title">Gender</div>
                      {this.state.gender_display == '' || this.state.gender_display == null? <div className="text hide-font">None</div> : <div className="text">{this.state.gender_display}</div> }
                    </li>
                    <li>
                      <div className="title">Marital Status</div>
                      {this.state.maritalStatus_display == '' || this.state.maritalStatus_display == null? <div className="text hide-font">None</div> : <div className="text">{this.state.maritalStatus_display}</div> }
                    </li>
                    <li>
                      <div className="title">Preferred Language</div>
                      {this.state.prefLanguageDisplay_display == '' || this.state.prefLanguageDisplay_display == null? <div className="text hide-font">None</div> : <div className="text">{this.state.prefLanguageDisplay_display}</div> }
                    </li>*/}
                  </ul>
                </div>
              </div>

              {/* Staff Profile Info Modal */}
              <div id="ProfileTab_profile_modal" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Staff Profile Information</h5>
                      <button type="button" className="close" id="close_btn_profile" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="card">
                          <div className="card-body">
                             <div className="row">
                              <div className="col-md-12">
                                <div className="form-group">
                                  <label>Preferred Name<span className="text-danger">*</span></label>
                                  <input type="text" className="form-control" value={this.state.Preferred_Name_Model} onChange={this.handleChange('Preferred_Name_Model')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["Preferred_Name_Model"]}</span>
                                </div>
                              </div>
                              {/*<div className="col-md-6">
                                <div className="form-group">
                                  <label>Date of Birth<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date" value={this.state.Date_of_Birth_Model} onChange={this.handleChange('Date_of_Birth_Model')}/>
                                  <span className="form-text error-font-color">{this.state.errormsg["Date_of_Birth_Model"]}</span>
                                  
                                </div>
                              </div>*/}
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>First Name<span className="text-danger">*</span></label>
                                  <input className="form-control" type="text"  value={this.state.First_Name_Model} onChange={this.handleChange('First_Name_Model')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["First_Name_Model"]}</span>
                                </div>
                              </div>
                              {/*<div className="col-md-4">
                                <div className="form-group">
                                  <label>Middle Name</label>
                                  <div className="form-group">
                                    <input className="form-control" type="text" value={this.state.Middle_Name_Model} onChange={this.handleChange('Middle_Name_Model')} />
                                    <span className="form-text error-font-color">{this.state.errormsg["Middle_Name_Model"]}</span>
                                  </div>
                                </div>
                              </div>*/}
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Last Name<span className="text-danger">*</span></label>
                                  <input className="form-control" type="text" value={this.state.Last_Name_Model} onChange={this.handleChange('Last_Name_Model')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["Last_Name_Model"]}</span>
                                </div>
                              </div>
                              {/*<div className="col-md-6">
                                <div className="form-group">
                                  <label>Gender<span className="text-danger">*</span></label>
                                  <select className="select form-control" value={this.state.Gender_Model} onChange={this.handleChange('Gender_Model')} >
                                    <option value="0">-</option>
                                    {this.state.gendersList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.name}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["Gender_Model"]}</span>
                                </div>
                              </div>
                              <div className="col-md-6">
                                <div className="form-group">
                                  <label>Marital status <span className="text-danger">*</span></label>
                                  <select className="select form-control" value={this.state.Marital_Status_Model} onChange={this.handleChange('Marital_Status_Model')} >
                                    <option value="0">-</option>
                                    {this.state.familiyStatusList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.name}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["Marital_Status_Model"]}</span>
                                </div>
                              </div>*/} 
                              {/*<div className="col-md-6">
                                <div className="form-group">
                                  <label>Preferred Language<span className="text-danger">*</span></label>
                              */}  
                                  {/*<Multiselect
                                    options={this.state.prefLanguagesList} // Options to display in the dropdown
                                    selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                                    onSelect={this.onSelect} // Function will trigger on select event
                                    onRemove={this.onRemove} // Function will trigger on remove event
                                    displayValue="name" // Property name to display in the dropdown options
                                    />*/}
                                  {/*<select className="form-control" value={this.state.Preferred_Language_Model}  onChange={this.handleChange('Preferred_Language_Model')}>
                                    <option value="">-</option>
                                    {this.state.prefLanguagesList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.name}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["Preferred_Language_Model"]}</span>
                                </div>
                              </div> */}
                            </div>  
                            <div className="submit-section">
                              <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateUserProfileTabProfileModel_API({})}>Submit</button>
                            </div>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            {/* /Staff Profile Info Modal */}
            </div>
        
      );
   }
}

export default ProfileView;
