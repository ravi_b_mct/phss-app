/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
//import {Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16 } from '../../../../Entryfile/imagepath'

import InputMask from 'react-input-mask';
import moment from 'moment';


import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Multiselect } from 'multiselect-react-dropdown';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import SystemHelpers from '../../../Helpers/SystemHelper';

import Datetime from "react-datetime";

class PersonalInformation extends Component {
  constructor(props) {
    super(props);

    //this.state = { Preferred_Language_Model: [] };
    this.state = { Preferred_Language_Model: '' };

    this.state = {
        errormsg :  '',
        user_role: [],
        all_data :this.props.all_data,

        // Extended Profile -> Personal Information mode
        Marital_Status_Model:'',
        Date_of_Birth :'',
        Preferred_Name_Model:'',
        SIN_Model :'',
        SIN_Expiry_Date_Model :'',
        Work_Permit_Model :'',
        Work_Permit_Expiry_Model :'',
        OHIP_Coverage_Model :'',
        Health_Number_Model :'',
        Health_Card_Expiry_Model :'',
        Other_Coverage_Model :'',
        Insurer_Name_Model :'',
        Coverage_Number_Model :'',
        Pay_Rate_Model :'',
        Salary_Model :'',

        prefLanguagesList: [],
        selectedValue: [],
        familiyStatusList:[],
        salaryList:[],
        gendersList:[],
        role_personal_info_can: {},

        gendersList:[],
        Gender_Model :  '',

        dateOfBirth : '',
        healthCardExpDate : '',
        workPermitExpNumber : '',

        // Display
        dateOfBirthDisplay : '',
        maritalStatusDisplay : '',
        genderDisplay : '',
        prefLanguageDisplay : '',
        personalEmailDisplay : '',
        workPermitNumberDisplay : '',
        workPermitExpNumberDisplay : '',
        ohipCoverageDisplay : '',
        healthNumberDisplay : '',
        healthCardVersionDisplay : '',
        healthCardExpDateDisplay : '',
        otherCoverageDisplay : '',
        isurerNameDisplay : '',
        coverageNumberDisplay : '',

        // Display 

        healthCardExpDateDisabled : true,
        workPermitExpNumberDisabled : true,
        staffContactFullname : localStorage.getItem('fullName')

        // Extended Profile -> Personal Information mode
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.onSelect = this.onSelect.bind(this);
    this.onRemove = this.onRemove.bind(this);

    this.handleEditdateOfBirth = this.handleEditdateOfBirth.bind(this);
    this.handleEditworkPermitExpNumber = this.handleEditworkPermitExpNumber.bind(this);
    this.handleEdithealthCardExpDate = this.handleEdithealthCardExpDate.bind(this);
  }

  handleEditdateOfBirth = (date) =>{
    //alert(date);
    console.log('dateOfBirth => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
    this.setState({ dateOfBirth : date });
  };


  handleEditworkPermitExpNumber = (date) =>{
    //alert(date);
    console.log('workPermitExpNumber => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
    this.setState({ workPermitExpNumber : date });
  };

  handleEdithealthCardExpDate = (date) =>{
    //alert(date);
    console.log('healthCardExpDate => '+ date);
    //console.log('AddTimeOut_Max => '+ NewDate);
    this.setState({ healthCardExpDate : date });
  };

  validationDate = (currentDate) => {
    //min={this.state.AddTimeIn_Min} max={this.state.AddTimeIn_Max}
    return currentDate.isBefore(moment(new Date()));
  };
  

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    
    // ================ Form change event ================ //
    // OHIP coverage
    if([input]=="ohipCoverage")
    {
      //alert(e.target.value);
      if(e.target.value == "true" || e.target.value == true )
      {
        //alert('if 123');
        $('#healthNumber').prop('disabled', false);
        $('#healthCardVersion').prop('disabled', false);
        //$('#healthCardExpDate').prop('disabled', false);
        this.setState({ healthCardExpDateDisabled : false });
        
      }
      else if(e.target.value == "false" || e.target.value == false )
      {
        //alert('else 123');
        $('#healthNumber').prop('disabled', true);
        $('#healthCardVersion').prop('disabled', true);
        //$('#healthCardExpDate').prop('disabled', true);
        this.setState({ healthCardExpDateDisabled : true });

        this.setState({ healthNumber : "" });
        this.setState({ healthCardVersion : "" });
        this.setState({ healthCardExpDate : "" });
      }
    }
    // OHIP coverage
    // Work Permit
    if([input]=="workPermitNumber")
    {
      if(e.target.value != "" &&  e.target.value.trim().length > 0)
      {
        //$('#workPermitExpNumber').prop('disabled', false);
        this.setState({ workPermitExpNumberDisabled : false });
      }
      else
      {
        //$('#workPermitExpNumber').prop('disabled', true);
        this.setState({ workPermitExpNumberDisabled : true });
        this.setState({ workPermitExpNumber : "" });
      }
    }
    // Work Permit
    // other coverage
    if([input]=="otherCoverage")
    {
      if(e.target.value == 'true')
      {
        $('#isurerName').prop('disabled', false);
        $('#coverageNumber').prop('disabled', false);
      }
      else
      {
        $('#isurerName').prop('disabled', true);
        $('#coverageNumber').prop('disabled', true);

        this.setState({ isurerName : "" });
        this.setState({ coverageNumber : "" });
      }
    }
    // other coverage
    // ================ Form change event ================ //
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

   // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
      toast.success(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }

  ToastError(msg){
      toast.error(msg, {
        position: "top-right",
        autoClose: process.env.API_TOAST_TIME,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: false,
        draggable: false
      });
  }
  // toast hide show method

  push_func(){
    this.props.history.push("/");
    this.hideLoader();
  }

  SessionOut(){
    this.showLoader();
  
    localStorage.removeItem("token");
    localStorage.removeItem("contactId");
    localStorage.removeItem("eMailAddress");
    localStorage.removeItem("fullName");

    //this.ToastError('Session Timeout');
    
    setTimeout( () => {this.push_func()}, 5000);
    
    return null;
  }

  componentDidMount() {

    /* Role Management */
     console.log('Role Store personal_info_can');
     /*var getrole = SystemHelpers.GetRole();
     let personal_info_can = getrole.personal_info_can;
     this.setState({ role_personal_info_can: personal_info_can });
     console.log(personal_info_can);*/
     console.log(this.props.personal_info_can);
     let personal_info_can = this.props.personal_info_can;
     this.setState({ role_personal_info_can: this.props.personal_info_can });
    /* Role Management */


    console.log("didmount PersonalInformation");
    console.log(this.state.all_data);

    this.DisplayFuncation(this.state.all_data);
  }

  DisplayFuncation(all_data){

    console.log('DisplayFuncation PersonalInformation');
    console.log(all_data);


    // Edit Time
    //this.setState({ dateOfBirth: moment(all_data.dateOfBirth).format('YYYY-MM-DD') });
    if(all_data.dateOfBirth != "" || all_data.dateOfBirth != null){
      this.setState({ dateOfBirth: moment(all_data.dateOfBirth,process.env.API_DATE_FORMAT) });
    }else{
      this.setState({ dateOfBirth: "" });
    }
    
    this.setState({ maritalStatus: all_data.maritalStatus });
    this.setState({ Gender_Model: all_data.gender });
    this.setState({ personalEmail: all_data.personalEmail });
    
    //this.setState({ sin: all_data.sin });
    //this.setState({ sinExpDate: moment(all_data.sinExpDate).format('YYYY-MM-DD') });

    this.setState({ workPermitNumber: all_data.workPermitNumber });
    //this.setState({ workPermitExpNumber: moment(all_data.workPermitExpNumber).format('YYYY-MM-DD') });
    if(all_data.workPermitExpNumber != "" && all_data.workPermitExpNumber != null){
      this.setState({ workPermitExpNumber: moment(all_data.workPermitExpNumber,process.env.API_DATE_FORMAT) });
    }else{
      this.setState({ workPermitExpNumber: "" });
    }
    

    this.setState({ ohipCoverage: all_data.ohipCoverage });
    this.setState({ healthNumber: all_data.healthNumber });
    this.setState({ healthCardVersion: all_data.healthCardVersion });
    //this.setState({ healthCardExpDate: moment(all_data.healthCardExpDate).format('YYYY-MM-DD') });
    
    if(all_data.healthCardExpDate != "" && all_data.healthCardExpDate != null){
      this.setState({ healthCardExpDate: moment(all_data.healthCardExpDate,process.env.API_DATE_FORMAT) });
    }else{
      this.setState({ healthCardExpDate: "" });
    }
    
    this.setState({ otherCoverage: all_data.otherCoverage });
    this.setState({ isurerName: all_data.isurerName });
    this.setState({ coverageNumber: all_data.coverageNumber });
    this.setState({ payRate: all_data.payRate });
    this.setState({ salary: all_data.salary });

    this.setState({ Preferred_Name_Model: all_data.preferredName });
    this.setState({ familiyStatusList: all_data.familiyStatusList });
    this.setState({ salaryList: all_data.salaryList });
    this.setState({ gendersList: all_data.gendersList });
    
    // Edit Time Show "Preferred Language"
    this.setState({ prefLanguagesList: all_data.prefLanguagesList });

    /*var all_list_pref_lang = all_data.prefLanguagesList;
    var selected_id_pref_lang = all_data.prefLanguageUpdatedValue;
    let selected_list_pref_lang = [];
    for (var i = 0; i<all_list_pref_lang.length; i++)
    {
      if(selected_id_pref_lang != null && selected_id_pref_lang.length!=0 )
      {
        for (var j = 0; j<selected_id_pref_lang.length; j++)
        {
          if(all_list_pref_lang[i].id==selected_id_pref_lang[j])
          {
            selected_list_pref_lang.push(all_list_pref_lang[i]);
          }
        }
      }
    }
    this.setState({ selectedValue: selected_list_pref_lang });*/
    //this.setState({ Preferred_Language_Model: selected_id_pref_lang });
    this.setState({ Preferred_Language_Model: all_data.prefLanguageDisplay });
    // Edit Time Show "Preferred Language"

    // ================ page load time ================ //
    // OHIP coverage
    if(all_data.ohipCoverage == true)
    {
      $('#healthNumber').prop('disabled', false);
      $('#healthCardVersion').prop('disabled', false);
      //$('#healthCardExpDate').prop('disabled', false);
      this.setState({ healthCardExpDateDisabled : false });
    }
    else
    {
      $('#healthNumber').prop('disabled', true);
      $('#healthCardVersion').prop('disabled', true);
      //$('#healthCardExpDate').prop('disabled', true);
      this.setState({ healthCardExpDateDisabled : true });

      this.setState({ healthNumber : "" });
      this.setState({ healthCardVersion : "" });
      this.setState({ healthCardExpDate : "" });
    }
    // OHIP coverage
    // Work Permit
    if(all_data.workPermitNumber != null && all_data.workPermitNumber != "" &&  all_data.workPermitNumber.trim().length > 0)
    {
      //$('#workPermitExpNumber').prop('disabled', false);
      this.setState({ workPermitExpNumberDisabled : false });
    }
    else
    {
      //$('#workPermitExpNumber').prop('disabled', true);
      this.setState({ workPermitExpNumberDisabled : true });
      this.setState({ workPermitExpNumber : "" });
    }
    // Work Permit
    // other coverage  otherCoverage isurerName coverageNumber
    if(all_data.otherCoverage == true)
    {
      $('#isurerName').prop('disabled', false);
      $('#coverageNumber').prop('disabled', false);
    }
    else
    {
      $('#isurerName').prop('disabled', true);
      $('#coverageNumber').prop('disabled', true);

      this.setState({ isurerName : "" });
      this.setState({ coverageNumber : "" });
    }
    // other coverage
    // ================ page load time ================ //


    // Display
    if(all_data.dateOfBirth != "" && all_data.dateOfBirth != null){
      this.setState({ dateOfBirthDisplay: all_data.dateOfBirth });
      this.props.setPropState('DateofBirth', all_data.dateOfBirth);
    }else{
      this.setState({ dateOfBirthDisplay: "" });
      this.props.setPropState('DateofBirth', "");
    }

    this.setState({ maritalStatusDisplay: all_data.maritalStatus});
    this.setState({ genderDisplay: all_data.gender });
    this.setState({ prefLanguageDisplay: all_data.prefLanguageDisplay });
    this.setState({ personalEmailDisplay: all_data.personalEmail });
    this.setState({ workPermitNumberDisplay: all_data.workPermitNumber });

    if(all_data.workPermitExpNumber != "" && all_data.workPermitExpNumber != null){
      this.setState({ workPermitExpNumberDisplay: all_data.workPermitExpNumber });
    }else{
      this.setState({ workPermitExpNumberDisplay: "" });
    }
      
    this.setState({ ohipCoverageDisplay: all_data.ohipCoverage });
    this.setState({ healthNumberDisplay: all_data.healthNumber });
    this.setState({ healthCardVersionDisplay: all_data.healthCardVersion });

    if(all_data.healthCardExpDate != "" && all_data.healthCardExpDate != null){
      this.setState({ healthCardExpDateDisplay: all_data.healthCardExpDate });
    }else{
      this.setState({ healthCardExpDateDisplay: "" });
    }
    
    this.setState({ otherCoverageDisplay: all_data.otherCoverage });
    this.setState({ isurerNameDisplay: all_data.isurerName });
    this.setState({ coverageNumberDisplay: all_data.coverageNumber });
    

    // Display
  }

  //
  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+localStorage.getItem("contactId");
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {

            localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
            localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
            this.setState({ all_data: data.data});
            this.DisplayFuncation(data.data);
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  //

  //
  onSelect(selectedList, selectedItem) {
    let lang =[];

    for (var i = 0; i<selectedList.length; i++)
    {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
    }
    this.setState({ Preferred_Language_Model: lang });
  }

  onRemove(selectedList, removedItem)
  {
    let lang =[];
    for (var i = 0; i<selectedList.length; i++) {
      var number1 = selectedList[i].id;
      var number =  number1.toString();
      lang.push(number);
    }
    if(lang.length != 0){
      this.setState({ Preferred_Language_Model: lang });
    }
  }
  //

  // Personal Information model Update api
  UpdateUserExtendedTabPersonalModel_API = () => e => {  
      e.preventDefault();  

      console.log("dateOfBirth= "+this.state["dateOfBirth"]);
      console.log("maritalStatus= "+this.state["maritalStatus"]);
      console.log("Gender_Model= "+this.state["Gender_Model"]);
      console.log("Preferred_Language_Model= "+this.state["Preferred_Language_Model"]);
      console.log("personalEmail= "+this.state["personalEmail"]);
      console.log("workPermitNumber= "+this.state["workPermitNumber"]);
      console.log("workPermitExpNumber= "+this.state["workPermitExpNumber"]);
      console.log("ohipCoverage= "+this.state["ohipCoverage"]);
      console.log("healthNumber= "+this.state["healthNumber"]);
      console.log("healthCardVersion= "+this.state["healthCardVersion"]);
      console.log("healthCardExpDate= "+this.state["healthCardExpDate"]);
      
      console.log("otherCoverage= "+$('#otherCoverage').val());
      console.log("isurerName= "+this.state["isurerName"]);
      console.log("coverageNumber= "+this.state["coverageNumber"]);
      //debugger;
      //alert($('#Extended_Marital_Status_Model').val());
      
      let step1Errors = {};
      //alert(this.state["Gender_Model"]);
      //return false;
      if (this.state["dateOfBirth"] == '' || this.state["dateOfBirth"] == null) {
        step1Errors["dateOfBirth"] = "Date of Birth is mandatory";
      }
      
      if (this.state["maritalStatus"] =='') {
        step1Errors["maritalStatus"] = "Marital Status is mandatory"
      }

      if (this.state["Gender_Model"] == '' || this.state["Gender_Model"] == null || this.state["Gender_Model"] == 0) {
        step1Errors["Gender_Model"] = "Gender is mandatory"
      }

      /*if (this.state["prefLanguagesList"] =='' || this.state["prefLanguagesList"] == null) {
        step1Errors["prefLanguagesList"] = "Preferred Language is mandatory"
      }*/
      
      if (this.state["Preferred_Language_Model"] =='' || this.state["Preferred_Language_Model"] == null) {
        step1Errors["Preferred_Language_Model"] = "Preferred Language is mandatory"
      }

      /*if (this.state["personalEmail"] === '' || this.state["personalEmail"] == null) {
        step1Errors["personalEmail"] = "Email is mandatory"
      }else if (typeof this.state["personalEmail"] !== "undefined" && this.state["personalEmail"] != null) {
        let lastAtPos = this.state["personalEmail"].lastIndexOf('@');
        let lastDotPos = this.state["personalEmail"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["personalEmail"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["personalEmail"].length - lastDotPos) > 2)) {
            step1Errors["personalEmail"] = "Email is not valid";
        }
      }*/

      if (typeof this.state["personalEmail"] !== "undefined" && this.state["personalEmail"] != null && this.state["personalEmail"] != '') {
        let lastAtPos = this.state["personalEmail"].lastIndexOf('@');
        let lastDotPos = this.state["personalEmail"].lastIndexOf('.');

        if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state["personalEmail"].indexOf('@@') == -1 && lastDotPos > 2 && (this.state["personalEmail"].length - lastDotPos) > 2)) {
            step1Errors["personalEmail"] = "Email is not valid";
        }
      }
      
      

      
      /*if (this.state["sin"] === '') {
        step1Errors["sin"] = "SIN is mandatory"
      }
      
      if (this.state["sinExpDate"] === '') {
        step1Errors["sinExpDate"] = "SIN Expiry Date is mandatory"
      }
      
      if (this.state["workPermitNumber"] === '') {
        step1Errors["workPermitNumber"] = "Work Permit is mandatory"
      }
      
      if (this.state["workPermitExpNumber"] === '') {
        step1Errors["workPermitExpNumber"] = "Work Permit Expiry is mandatory"
      }*/
      //alert(this.state["ohipCoverage"]);
      //alert(this.state["ohipCoverage"]);
      //return false;
      //alert(this.state["ohipCoverage"]);
      //if (this.state["ohipCoverage"] == "" && this.state["ohipCoverage"] != false) {
      if (this.state["ohipCoverage"] != false && this.state["ohipCoverage"] != true && (this.state["ohipCoverage"] == "" || this.state["ohipCoverage"] == null)) {
        step1Errors["ohipCoverage"] = "OHIP Coverage is mandatory"
      }
      
      /*if (this.state["healthNumber"] === '') {
        step1Errors["healthNumber"] = "Health Card Number is mandatory"
      }*/
      //alert(this.state["ohipCoverage"]);
      //alert(this.state["healthCardExpDate"]);
      if(this.state["ohipCoverage"] == true || this.state["ohipCoverage"] == "true" )
      {
        //alert("if api");
        if (this.state["healthCardExpDate"] == "" || this.state["healthCardExpDate"] == null) {
          //alert("if 1 api");
          step1Errors["healthCardExpDate"] = "Health Card Expiry is mandatory"
        }else{
          //alert("else 2 api");
        }
      }
      
      
      /*if (this.state["otherCoverage"] == '') {
        step1Errors["otherCoverage"] = "Other Coverage is mandatory"
      }
      
      if (this.state["isurerName"] === '') {
        step1Errors["isurerName"] = "Insurer Nameis mandatory"
      }
      
      if (this.state["coverageNumber"] === '') {
        step1Errors["coverageNumber"] = "Coverage Number is mandatory"
      }
      
      if (this.state["payRate"] === '') {
        step1Errors["payRate"] = "Pay Rateis mandatory"
      }

      if (this.state["salary"] === '') {
        step1Errors["salary"] = "Salary is mandatory"
      }*/
      
    
      
      console.log(step1Errors);
      this.setState({ errormsg: step1Errors });
        
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      this.showLoader();
      //alert(this.state["workPermitNumber"]);
      //alert(this.state["healthCardExpDate"]);
      //return false;

      var dateOfBirth=moment(this.state["dateOfBirth"]).format('MM-DD-YYYY');
      //var sinExpDate=moment(this.state["sinExpDate"]).format('MM-DD-YYYY');

      var workPermitExpNumber= "";
      if(this.state["workPermitExpNumber"] != "" && this.state["workPermitExpNumber"] != null){
        workPermitExpNumber=moment(this.state["workPermitExpNumber"]).format('MM-DD-YYYY');
      }
      
      
      var healthCardExpDate = "";
      if(this.state["healthCardExpDate"] !="" && this.state["healthCardExpDate"] !=null){
        healthCardExpDate=moment(this.state["healthCardExpDate"]).format('MM-DD-YYYY');
      }


      /* *************************************************************************** */
        if(this.state["maritalStatus"] != '' && this.state["maritalStatus"] != null){
          var maritalStatus = this.state["maritalStatus"].trim();  
        }else{
          var maritalStatus = this.state["maritalStatus"];
        }

        if(this.state["personalEmail"] != '' && this.state["personalEmail"] != null){
          var personalEmail = this.state["personalEmail"].trim();  
        }else{
          var personalEmail = this.state["personalEmail"];
        }

        if(this.state["Preferred_Language_Model"] != '' && this.state["Preferred_Language_Model"] != null){
          var Preferred_Language_Model = this.state["Preferred_Language_Model"].trim();  
        }else{
          var Preferred_Language_Model = this.state["Preferred_Language_Model"];
        }

        if(this.state["workPermitNumber"] != '' && this.state["workPermitNumber"] != null){
          var workPermitNumber = this.state["workPermitNumber"].trim();  
        }else{
          var workPermitNumber = this.state["workPermitNumber"];
        }

        var ohipCoverage = this.state["ohipCoverage"];

        if(this.state["healthNumber"] != '' && this.state["healthNumber"] != null){
          var healthNumber = this.state["healthNumber"].trim();  
        }else{
          var healthNumber = this.state["healthNumber"];
        }

        if(healthCardExpDate != '' && healthCardExpDate != null){
          var healthCardExpDate = healthCardExpDate.trim();  
        }else{
          var healthCardExpDate = healthCardExpDate;
        }

        if(this.state["isurerName"] != '' && this.state["isurerName"] != null){
          var isurerName = this.state["isurerName"].trim();  
        }else{
          var isurerName = this.state["isurerName"];
        }

        if(this.state["coverageNumber"] != '' && this.state["coverageNumber"] != null){
          var coverageNumber = this.state["coverageNumber"].trim();  
        }else{
          var coverageNumber = this.state["coverageNumber"];
        }

        if(this.state["healthCardVersion"] != '' && this.state["healthCardVersion"] != null){
          var healthCardVersion = this.state["healthCardVersion"].trim();  
        }else{
          var healthCardVersion = this.state["healthCardVersion"];
        }

        if(this.state["Gender_Model"] != '' && this.state["Gender_Model"] != null){
          var Gender_Model = this.state["Gender_Model"].trim();  
        }else{
          var Gender_Model = this.state["Gender_Model"];
        }

      /* *************************************************************************** */
      
      
      

      let User_extended_profile_contact = {
         dateOfBirth: dateOfBirth,
         //dateOfBirth: moment(dateOfBirth, "MM-DD-YYYY"),
         maritalStatus: maritalStatus,
         //dateOfBirth: "",
         //maritalStatus: "",

         personalEmail: personalEmail,
         //prefLanguageUpdatedValue: this.state["Preferred_Language_Model"],
         prefLanguageDisplay: Preferred_Language_Model,
         
         // sin: this.state["sin"],
         // sinExpDate: moment(sinExpDate, "MM-DD-YYYY").add(1, 'days'),
         sin: "",
         sinExpDate: "",

         workPermitNumber: workPermitNumber,

         //workPermitExpNumber: moment(workPermitExpNumber, "MM-DD-YYYY").add(1, 'days'),
         workPermitExpNumber: workPermitExpNumber,

         ohipCoverage: ohipCoverage,
         healthNumber: healthNumber,
         //healthCardExpDate: this.state["healthCardExpDate"],
         
         //healthCardExpDate: moment(healthCardExpDate, "MM-DD-YYYY").add(1, 'days'),
         healthCardExpDate: healthCardExpDate,

         otherCoverage: $('#otherCoverage').val(),
         isurerName: isurerName,
         coverageNumber: coverageNumber,

         // payRate: this.state["payRate"],
         // salary: this.state["salary"],
         payRate: "",
         salary: "",

         healthCardVersion: healthCardVersion,
         Gender: Gender_Model

      };

      console.log("UpdateUserPersonalProfile Payroll");
      console.log(User_extended_profile_contact);
      //return false;

      let bodyarray = {};
      bodyarray["contactId"] = localStorage.getItem("contactId");
      bodyarray["userBasicInfo"] = User_extended_profile_contact;
      bodyarray["userName"] = this.state.staffContactFullname;
      
      var url=process.env.API_API_URL+'UpdateUserPersonalProfile';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
        console.log("responseJson UpdateUserPersonalProfile");
        console.log(data);
        //console.log(responseJson);
        // debugger;

        if (data.responseType === "1") {
          //this.ToastSuccess(data.responseMessge); 
          this.GetProfile(); 
          this.ToastSuccess('Contact updated successfully');
          $( ".close" ).trigger( "click" );
          //alert('11111111111111');
        }
        else{
          this.ToastError(data.responseMessge);
        }
        this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }
  // Personal Information model Update api

  ClearRecord = ()=> e => {
    //alert('clear');
    e.preventDefault();

    //this.DisplayFuncation(this.state.all_data);

    //console.log('clear');
    //console.log(this.state.all_data);

    this.setState({ errormsg: '' });

    // Model
    if(this.state.all_data.dateOfBirth != "" && this.state.all_data.dateOfBirth != null){
      this.setState({ dateOfBirth: moment(this.state.all_data.dateOfBirth) });
    }else{
      this.setState({ dateOfBirth: "" });
    }

    if(this.state.all_data.maritalStatus != "" && this.state.all_data.maritalStatus != null){
      this.setState({ maritalStatus: this.state.all_data.maritalStatus });
    }else{
      this.setState({ maritalStatus: "" });
    }

    if(this.state.all_data.gender != "" && this.state.all_data.gender != null){
      this.setState({ Gender_Model: this.state.all_data.gender });
    }else{
      this.setState({ Gender_Model: "" });
    }

    if(this.state.all_data.prefLanguageDisplay != "" && this.state.all_data.prefLanguageDisplay != null){
      this.setState({ Preferred_Language_Model: this.state.all_data.prefLanguageDisplay });
    }else{
      this.setState({ Preferred_Language_Model: "" });
    }
    
    if(this.state.all_data.personalEmail != "" && this.state.all_data.personalEmail != null){
      this.setState({ personalEmail: this.state.all_data.personalEmail });
    }else{
      this.setState({ personalEmail: "" });
    }

    if(this.state.all_data.workPermitNumber != "" && this.state.all_data.workPermitNumber != null){
      this.setState({ workPermitNumber: this.state.all_data.workPermitNumber });
    }else{
      this.setState({ workPermitNumber: "" });
    }

    if(this.state.all_data.workPermitExpNumber != "" && this.state.all_data.workPermitExpNumber != null){
      this.setState({ workPermitExpNumber: moment(this.state.all_data.workPermitExpNumber) });
    }else{
      this.setState({ workPermitExpNumber: "" });
    }

    this.setState({ ohipCoverage: this.state.all_data.ohipCoverage });

    if(this.state.all_data.healthNumber != "" && this.state.all_data.healthNumber != null){
      this.setState({ healthNumber: this.state.all_data.healthNumber });
    }else{
      this.setState({ healthNumber: "" });
    }

    this.setState({ healthCardVersion: this.state.all_data.healthCardVersion });

    if(this.state.all_data.healthCardExpDate != "" && this.state.all_data.healthCardExpDate != null){
      this.setState({ healthCardExpDate: moment(this.state.all_data.healthCardExpDate) });
    }else{
      this.setState({ healthCardExpDate: "" });
    }

    this.setState({ otherCoverage: this.state.all_data.otherCoverage });

    if(this.state.all_data.isurerName != "" && this.state.all_data.isurerName != null){
      this.setState({ isurerName: this.state.all_data.isurerName });
    }else{
      this.setState({ isurerName: "" });
    }

    if(this.state.all_data.coverageNumber != "" && this.state.all_data.coverageNumber != null){
      this.setState({ coverageNumber: this.state.all_data.coverageNumber });
    }else{
      this.setState({ coverageNumber: "" });
    }
    // Model

    //

    //Display
    //Display

    // Drop Down
    this.setState({ familiyStatusList: this.state.all_data.familiyStatusList });
    this.setState({ gendersList: this.state.all_data.gendersList });
    this.setState({ prefLanguagesList: this.state.all_data.prefLanguagesList });
    // Drop Down
    
    // Hide Model  
    this.setState({ sin: this.state.all_data.sin });
    this.setState({ sinExpDate: moment(this.state.all_data.sinExpDate).format('YYYY-MM-DD') });

    this.setState({ payRate: this.state.all_data.payRate });
    this.setState({ salary: this.state.all_data.salary });

    this.setState({ Preferred_Name_Model: this.state.all_data.preferredName });
    
    this.setState({ salaryList: this.state.all_data.salaryList });
    // Hide Model
    
    
    // Edit Time Show "Preferred Language"


    // ================ page load time ================ //
    // OHIP coverage
    if(this.state.all_data.ohipCoverage == "true" || this.state.all_data.ohipCoverage == true)
    {
      $('#healthNumber').prop('disabled', false);
      $('#healthCardVersion').prop('disabled', false);
      //$('#healthCardExpDate').prop('disabled', false);
      this.setState({ healthCardExpDateDisabled : false });

      //this.setState({ healthCardExpDate: moment(this.state.all_data.healthCardExpDate) });
    }
    else if(this.state.all_data.ohipCoverage == "false" || this.state.all_data.ohipCoverage == false)
    {
      $('#healthNumber').prop('disabled', true);
      $('#healthCardVersion').prop('disabled', true);
      //$('#healthCardExpDate').prop('disabled', true);
      this.setState({ healthCardExpDateDisabled : true });

      this.setState({ healthNumber : "" });
      this.setState({ healthCardVersion : "" });
      this.setState({ healthCardExpDate : "" });
    }
    // OHIP coverage

    // Work Permit
    if(this.state.all_data.workPermitNumber != "" &&  this.state.all_data.workPermitNumber.trim().length > 0)
    {
      //$('#workPermitExpNumber').prop('disabled', false);
      this.setState({ workPermitExpNumberDisabled : false });

      //this.setState({ workPermitExpNumber: moment(this.state.all_data.workPermitExpNumber) });
    }
    else
    {
      //$('#workPermitExpNumber').prop('disabled', true);
      this.setState({ workPermitExpNumberDisabled : true });

      this.setState({ workPermitExpNumber : "" });
    }
    // Work Permit
    // other coverage  otherCoverage isurerName coverageNumber
    if(this.state.all_data.otherCoverage == true)
    {
      $('#isurerName').prop('disabled', false);
      $('#coverageNumber').prop('disabled', false);
    }
    else
    {
      $('#isurerName').prop('disabled', true);
      $('#coverageNumber').prop('disabled', true);

      this.setState({ isurerName : "" });
      this.setState({ coverageNumber : "" });
    }
    // other coverage
    // ================ page load time ================ //
    
  }

   render() {
     
      return (
        
        //console.log('render ', this.state.dateOfBirth),
        <div className="row">
          {(this.state.loading) ? <Loader /> : null}
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">
              <div className="card-body">
                {this.state.role_personal_info_can.personal_info_can_update == true ? 
                  <h3 className="card-title">Personal Information<a href="#" className="edit-icon" data-toggle="modal" data-target="#ExtendedProfileTab_personal_info_modal"><i className="fa fa-pencil" /></a></h3>
                  : <h3 className="card-title">Personal Information <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                <ul className="personal-info">
                  <li>
                    <div className="title">Date of Birth</div>
                    {this.state.dateOfBirthDisplay == '' || this.state.dateOfBirthDisplay == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.dateOfBirthDisplay,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                  </li>
                  <li>
                    <div className="title">Marital Status</div>
                    {this.state.maritalStatusDisplay == '' || this.state.maritalStatusDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.maritalStatusDisplay}</div> }
                  </li>
                  <li>
                      <div className="title">Gender</div>
                      {this.state.genderDisplay == '' || this.state.genderDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.genderDisplay}</div> }
                    </li>
                  <li>
                    <div className="title">Preferred Language</div>
                    {this.state.prefLanguageDisplay == '' || this.state.prefLanguageDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.prefLanguageDisplay}</div> }
                  </li>
                  <li>
                    <div className="title">Personal Email Address</div>
                    {this.state.personalEmailDisplay == '' || this.state.personalEmailDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.personalEmailDisplay}</div> }
                  </li>
                  {/*<li>
                    <div className="title">SIN</div>
                    {this.state.all_data.sin == '' || this.state.all_data.sin == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.sin}</div> }
                  </li>
                  <li>
                    <div className="title">SIN Expiry Date</div>
                    {this.state.all_data.sinExpDate == '' || this.state.all_data.sinExpDate == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.sinExpDate}</div> }
                  </li>*/}
                  <li>
                    <div className="title">Work Permit#</div>
                    {this.state.workPermitNumberDisplay == '' || this.state.workPermitNumberDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.workPermitNumberDisplay}</div> }
                  </li>
                  <li>
                    <div className="title">Work Permit Expiry</div>
                    {this.state.workPermitExpNumberDisplay == '' || this.state.workPermitExpNumberDisplay == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.workPermitExpNumberDisplay,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                  </li>
                  <li>
                    <div className="title">OHIP Coverage</div>
                    {this.state.ohipCoverageDisplay == true ? <div className="text">Yes</div> : <div className="text">No</div> }
                  </li>
                  <li>
                    <div className="title">Health Card Number</div>
                    {this.state.healthNumberDisplay == '' || this.state.healthNumberDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.healthNumberDisplay}</div> }
                  </li>
                  <li>
                    <div className="title">Heath Card Version</div>
                    {this.state.healthCardVersionDisplay == '' || this.state.healthCardVersionDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.healthCardVersionDisplay}</div> }
                  </li>
                  <li>
                    <div className="title">Health Card Expiry</div>
                    {this.state.healthCardExpDateDisplay == '' || this.state.healthCardExpDateDisplay == null? <div className="text hide-font">None</div> : <div className="text">{moment(this.state.healthCardExpDateDisplay,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT)}</div> }
                  </li>
                  <li>
                    <div className="title">Other Coverage</div>
                    {this.state.otherCoverageDisplay == true ? <div className="text">Yes</div> : <div className="text">No</div> }
                  </li>
                  <li>
                    <div className="title">Insurer Name</div>
                    {this.state.isurerNameDisplay == '' || this.state.isurerNameDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.isurerNameDisplay}</div> }
                  </li>
                  <li>
                    <div className="title">Coverage Number</div>
                    {this.state.coverageNumberDisplay == '' || this.state.coverageNumberDisplay == null? <div className="text hide-font">None</div> : <div className="text">{this.state.coverageNumberDisplay}</div> }
                  </li>
                  {/*<li>
                    <div className="title">Pay Rate</div>
                    {this.state.all_data.payRate == '' || this.state.all_data.payRate == null? <div className="text hide-font">None</div> : <div className="text">{this.state.all_data.payRate}</div> }
                  </li>
                  <li>
                    <div className="title">Salary</div>
                    {this.state.all_data.salary == '' || this.state.all_data.salary == null? <div className="text hide-font">None</div> : <div className="text">{this.state.salary}</div> }
                  </li>*/} 
                </ul>
              </div>
            </div>
          </div>

          {/* Extended Profile Modal */}
            <div id="ExtendedProfileTab_personal_info_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Personal Information</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date of Birth<span className="text-danger">*</span></label>
                                  <Datetime    
                                      isValidDate={this.validationDate}                              
                                      inputProps={{readOnly: true}}
                                      closeOnTab={true}
                                      input={true}
                                      value={(this.state.dateOfBirth) ? this.state.dateOfBirth : ''}
                                      onChange={this.handleEditdateOfBirth}
                                      dateFormat={process.env.DATE_FORMAT}
                                      timeFormat={false}
                                      renderInput={(props) => {
                                         return <input {...props} value={(this.state.dateOfBirth) ? props.value : ''} />
                                      }}
                                    />
                                  {/*<input type="date" className="form-control" value={this.state.dateOfBirth}  onChange={this.handleChange('dateOfBirth')}/>*/}
                                  <span className="form-text error-font-color">{this.state.errormsg["dateOfBirth"]}</span> 
                                
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Marital Status<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.maritalStatus}  onChange={this.handleChange('maritalStatus')}>
                                  <option value="">-</option>
                                  {this.state.familiyStatusList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["maritalStatus"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group">
                                  <label>Gender<span className="text-danger">*</span></label>
                                  <select className="select form-control" value={this.state.Gender_Model} onChange={this.handleChange('Gender_Model')} >
                                    <option value="">-</option>
                                    {this.state.gendersList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.name}>{listValue.name}</option>
                                      );
                                    })}
                                  </select>
                                  <span className="form-text error-font-color">{this.state.errormsg["Gender_Model"]}</span>
                                </div>
                              </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Preferred Language<span className="text-danger">*</span></label>
                                {/*<Multiselect
                                    options={this.state.prefLanguagesList} // Options to display in the dropdown
                                    selectedValues={this.state.selectedValue} // Preselected value to persist in dropdown
                                    onSelect={this.onSelect} // Function will trigger on select event
                                    onRemove={this.onRemove} // Function will trigger on remove event
                                    displayValue="name" // Property name to display in the dropdown options
                                    />*/}
                                <select className="form-control" value={this.state.Preferred_Language_Model}  onChange={this.handleChange('Preferred_Language_Model')}>
                                  <option value="">-</option>
                                  {this.state.prefLanguagesList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Preferred_Language_Model"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Personal Email Address</label>
                                <div className="form-group">
                                  <input className="form-control" type="text" value={this.state.personalEmail}  onChange={this.handleChange('personalEmail')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["personalEmail"]}</span> 
                                </div>
                              </div>
                            </div>
                            {/*<div className="col-md-6">
                              <div className="form-group">
                                <label>SIN</label>
                                <input className="form-control" type="text" value={this.state.sin} onChange={this.handleChange('sin')} />
                                <span className="form-text error-font-color">{this.state.errormsg["sin"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>SIN Expiry Date</label>
                                
                                  <input type="date" className="form-control" value={this.state.sinExpDate} onChange={this.handleChange('sinExpDate')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["sinExpDate"]}</span>
                                
                              </div>
                            </div>*/}
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Work Permit#</label>
                                <input className="form-control" type="text" id="workPermitNumber" value={this.state.workPermitNumber} onChange={this.handleChange('workPermitNumber')} />
                                <span className="form-text error-font-color">{this.state.errormsg["workPermitNumber"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Work Permit Expiry</label>
                                    <Datetime
                                      inputProps={{readOnly: this.state.workPermitExpNumberDisabled,disabled: this.state.workPermitExpNumberDisabled}}
                                      closeOnTab={true}
                                      input={true}
                                      value={(this.state.workPermitExpNumber) ? this.state.workPermitExpNumber : ''}
                                      onChange={this.handleEditworkPermitExpNumber}
                                      dateFormat={process.env.DATE_FORMAT}
                                      timeFormat={false}
                                      renderInput={(props) => {
                                         return <input {...props} value={(this.state.workPermitExpNumber) ? props.value : ''} />
                                      }}
                                    />
                                  {/*<input type="date" className="form-control" id="workPermitExpNumber" value={this.state.workPermitExpNumber} onChange={this.handleChange('workPermitExpNumber')} />*/}
                                  <span className="form-text error-font-color">{this.state.errormsg["workPermitExpNumber"]}</span> 
                                
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>OHIP Coverage<span className="text-danger">*</span></label>
                                <select className="form-control" id="ohipCoverage" value={this.state.ohipCoverage} onChange={this.handleChange('ohipCoverage')} >
                                  <option value="">-</option>
                                  <option value="true">Yes</option>
                                  <option value="false">No</option>
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["ohipCoverage"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Health Card Number</label>
                                <input type="text" className="form-control" id="healthNumber" maxlength="20" value={this.state.healthNumber} onChange={this.handleChange('healthNumber')} />
                                <span className="form-text error-font-color">{this.state.errormsg["healthNumber"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Heath Card Version</label>
                                <input type="text" className="form-control" id="healthCardVersion" maxlength="20" value={this.state.healthCardVersion} onChange={this.handleChange('healthCardVersion')} />
                                <span className="form-text error-font-color">{this.state.errormsg["healthCardVersion"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Health Card Expiry {this.state.ohipCoverage == "true" || this.state.ohipCoverage == true ? <span className="text-danger">*</span> : null } </label>
                                    <Datetime
                                      inputProps={{readOnly: this.state.healthCardExpDateDisabled,disabled: this.state.healthCardExpDateDisabled}}
                                      closeOnTab={true}
                                      input={true}
                                      value={(this.state.healthCardExpDate) ? this.state.healthCardExpDate : ''}
                                      onChange={this.handleEdithealthCardExpDate}
                                      dateFormat={process.env.DATE_FORMAT}
                                      timeFormat={false}
                                      renderInput={(props) => {
                                         return <input {...props} value={(this.state.healthCardExpDate) ? props.value : ''} />
                                      }}
                                    />
                                  {/*<input type="date" className="form-control" id="healthCardExpDate" value={this.state.healthCardExpDate} onChange={this.handleChange('healthCardExpDate')} />*/}
                                  <span className="form-text error-font-color">{this.state.errormsg["healthCardExpDate"]}</span> 
                                
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Other Coverage</label>
                                <select className="form-control" id="otherCoverage" value={this.state.otherCoverage} onChange={this.handleChange('otherCoverage')} >
                                  <option value="">-</option>
                                  <option value="true">Yes</option>
                                  <option value="false">No</option>
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["otherCoverage"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Insurer Name</label>
                                <input type="text" className="form-control" id="isurerName" value={this.state.isurerName} onChange={this.handleChange('isurerName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["isurerName"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Coverage Number</label>
                                <input type="text" className="form-control" id="coverageNumber" value={this.state.coverageNumber} onChange={this.handleChange('coverageNumber')} />
                                <span className="form-text error-font-color">{this.state.errormsg["coverageNumber"]}</span> 
                              </div>
                            </div>
                            {/*<div className="col-md-6">
                              <div className="form-group">
                                <label>Pay Rate</label>
                                <input type="text" className="form-control" value={this.state.payRate} onChange={this.handleChange('payRate')} />
                                <span className="form-text error-font-color">{this.state.errormsg["payRate"]}</span> 
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Salary</label>
                                <select className="form-control" value={this.state.salary} onChange={this.handleChange('salary')} >
                                  <option value="">-</option>
                                  {this.state.salaryList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["salary"]}</span> 
                              </div>
                            </div>*/}
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" type="submit" onClick={this.UpdateUserExtendedTabPersonalModel_API({})}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          {/*Extended Profile Modal */}
        </div>
      );
   }
}

export default PersonalInformation;
