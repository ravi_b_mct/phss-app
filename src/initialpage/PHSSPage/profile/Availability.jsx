/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';

import Header from '../../Sidebar/header.jsx';
import SidebarContent from '../../Sidebar/sidebar';

import Loader from '../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Datetime from "react-datetime";


import FullCalendar, { formatDate } from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import "./calendar.css"
import { now } from 'jquery';
import { Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";

class Availability extends Component {
  constructor(props) {
    super(props);

    this.state = {
        ListGrid:[],
        errormsg : '',
        all_data :this.props.all_data,
        user_role: [],
        DateList: [],
        DeleteList:{},
        staffContactID:this.props.staffContactID,

        AllLocationName:[],
        role_availability_can : {},
        
        AddEmployeeName:this.props.staffContactID,
        AddStartDate:'',
        AddEndDate:'',
        AddShift_Type:'',
        AddWork_Hours:'',
        CalendarLoad:false,
        submit_type:'',

        add_same_time_from : "",
        add_same_time_to : "",
        same_mandatory :'',
        same_sun : false,
        same_mon : false,
        same_tue : false,
        same_wed : false,
        same_thu : false,
        same_fri : false,
        same_sat : false,

        very_sun : false,
        very_mon : false,
        very_tue : false,
        very_wed : false,
        very_thu : false,
        very_fri : false,
        very_sat : false,

        add_very_sun_from: '',
        add_very_sun_to:'',
        add_very_mon_from: '',
        add_very_mon_to:'',
        add_very_tue_from: '',
        add_very_tue_to:'',
        add_very_wed_from: '',
        add_very_wed_to:'',
        add_very_thu_from: '',
        add_very_thu_to:'',
        add_very_fri_from: '',
        add_very_fri_to:'',
        add_very_sat_from: '',
        add_very_sat_to:'',


        role_locations_departments_can: {},

        isDelete : false,

        header_data : [],
        CRMRegisterUser : [],
        calendar_availability__list : [],




        iseditdelete : false,
        addneweventobj:null,
        isnewevent:false,
        event_title:"",
        category_color:'',
        weekendsVisible: true,
        currentEvents: [],
        defaultEvents :  [],

        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);

    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleAddEndDate = this.handleAddEndDate.bind(this);

    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    this.handleEditEndDate = this.handleEditEndDate.bind(this);
  }

  handleAddStartDate = (date) =>{
    console.log('AddStartDate => '+ date);
    this.setState({ AddStartDate : date });
    this.setState({ AddEndDate : '' });

    if(this.state.AddShift_Type !=''){
        this.GetUserAvailibityDateWise(date,this.state.AddShift_Type);
    }

    delete this.state.errormsg['AddStartDate'];
  };

  handleAddEndDate = (date) =>{
    console.log('AddEndDate => '+ date);
    this.setState({ AddEndDate : date });

    if(this.state.AddStartDate != '' && this.state.AddShift_Type !=''){
        this.GetUserAvailibityDateWise(this.state.AddStartDate,this.state.AddShift_Type);
    }

    delete this.state.errormsg['AddEndDate'];
  };

  handleEditStartDate = (date) =>{
    console.log('EditStartDate => '+ date);
    this.setState({ EditStartDate : date });
    this.setState({ EditEndDate : '' });
  };

  handleEditEndDate = (date) =>{
    console.log('EditEndDate => '+ date);
    this.setState({ EditEndDate : date });
  };

  validationAddEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddStartDate));
  };

  validationEditEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditStartDate));
  };

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {

    if(input !="very_sun" && input !="very_mon" && input !="very_tue" && input !="very_wed"  && input !="very_thu" && input !="very_fri" && input !="very_sat")
    {
      this.setState({ [input]: e.target.value });
    }
    console.log(input);
    console.log(e.target.value);
    delete this.state.errormsg[input];
    if (this.state[input] != '') {
        console.log('if');
        delete this.state.errormsg[input];
    }

    if(input == "AddShift_Type" && this.state.AddStartDate !=''){
      this.GetUserAvailibityDateWise(this.state.AddStartDate,e.target.value);
    }

    

    if(input == "AddShift_Type" && e.target.value == 'false'){
      this.setState({ AddWork_Hours: 'same_time_each' });
      this.setState({ same_mandatory: false });
      $( "#same_time_each" ).trigger( "click" );
      $("#very_by_day").prop("disabled", true);
      delete this.state.errormsg["AddWork_Hours"];
      delete this.state.errormsg["add_same_time_from"];
      delete this.state.errormsg["add_same_time_to"];

      delete this.state.errormsg["add_very_sun_from"];
      delete this.state.errormsg["add_very_sun_to"];

      delete this.state.errormsg["add_very_mon_from"];
      delete this.state.errormsg["add_very_mon_to"];


      delete this.state.errormsg["add_very_tue_from"];
      delete this.state.errormsg["add_very_tue_to"];

      delete this.state.errormsg["add_very_wed_from"];
      delete this.state.errormsg["add_very_wed_to"];

      delete this.state.errormsg["add_very_thu_from"];
      delete this.state.errormsg["add_very_thu_to"];

      delete this.state.errormsg["add_very_fri_from"];
      delete this.state.errormsg["add_very_fri_to"];

      delete this.state.errormsg["add_very_sat_from"];
      delete this.state.errormsg["add_very_sat_to"];

    }

    if(input == "AddShift_Type" && e.target.value == 'true'){
      $("#very_by_day").prop("disabled", false);
      this.setState({ same_mandatory: true });
    }

    if(input == "AddWork_Hours" && e.target.value == 'very_by_day'){
      $( "#day_shift" ).trigger( "click" );
      this.setState({ AddShift_Type: true });
      this.setState({ same_mandatory: true });

      this.Clear_Same_Day_Fun();
    }

    if(input == "AddWork_Hours" && e.target.value == 'same_time_each'){
      this.Clear_Very_Day_Fun();
    }

    


    if(input=="same_sun")
    {
      if(this.state.same_sun == true){
        this.setState({ same_sun: false });
      }else{
       this.setState({ same_sun: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }

    if(input=="same_mon")
    {
      if(this.state.same_mon == true){
        this.setState({ same_mon: false });
      }else{
       this.setState({ same_mon: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }


    if(input=="same_tue")
    {
      if(this.state.same_tue == true){
        this.setState({ same_tue: false });
      }else{
       this.setState({ same_tue: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }


    if(input=="same_wed")
    {
      if(this.state.same_wed == true){
        this.setState({ same_wed: false });
      }else{
       this.setState({ same_wed: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }


    if(input=="same_thu")
    {
      if(this.state.same_thu == true){
        this.setState({ same_thu: false });
      }else{
       this.setState({ same_thu: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }

    if(input=="same_fri")
    {
      if(this.state.same_fri == true){
        this.setState({ same_fri: false });
      }else{
       this.setState({ same_fri: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }

    if(input=="same_sat")
    {
      if(this.state.same_sat == true){
        this.setState({ same_sat: false });
      }else{
       this.setState({ same_sat: true }); 
      }
      delete this.state.errormsg["add_same_day"];
    }



    if(input=="very_sun")
    {
      if(this.state.very_sun == true){
        this.setState({ very_sun: false });

        this.setState({ add_very_sun_from: '' });
        this.setState({ add_very_sun_to: '' });
        
        delete this.state.errormsg["add_very_sun_from"];
        delete this.state.errormsg["add_very_sun_to"];
        
      }else{
       this.setState({ very_sun: true }); 
      }
    }

    if(input=="very_mon")
    {
      if(this.state.very_mon == true){
        this.setState({ very_mon: false });

        this.setState({ add_very_mon_from: '' });
        this.setState({ add_very_mon_to: '' });

        delete this.state.errormsg["add_very_mon_from"];
        delete this.state.errormsg["add_very_mon_to"];
        
      }else{
       this.setState({ very_mon: true }); 
      }
    }


    if(input=="very_tue")
    {
      if(this.state.very_tue == true){
        this.setState({ very_tue: false });

        this.setState({ add_very_tue_from: '' });
        this.setState({ add_very_tue_to: '' });

        delete this.state.errormsg["add_very_tue_from"];
        delete this.state.errormsg["add_very_tue_to"];
        
      }else{
       this.setState({ very_tue: true }); 
      }
    }

    if(input=="very_wed")
    {
      if(this.state.very_wed == true){
        this.setState({ very_wed: false });

        this.setState({ add_very_wed_from: '' });
        this.setState({ add_very_wed_to: '' });

        delete this.state.errormsg["add_very_wed_from"];
        delete this.state.errormsg["add_very_wed_to"];
        
      }else{
       this.setState({ very_wed: true }); 
      }
    }

    if(input=="very_thu")
    {
      if(this.state.very_thu == true){
        this.setState({ very_thu: false });

        this.setState({ add_very_thu_from: '' });
        this.setState({ add_very_thu_to: '' });

        delete this.state.errormsg["add_very_thu_from"];
        delete this.state.errormsg["add_very_thu_to"];
        
      }else{
       this.setState({ very_thu: true }); 
      }
    }

    if(input=="very_fri")
    {
      if(this.state.very_fri == true){
        this.setState({ very_fri: false });

        this.setState({ add_very_fri_from: '' });
        this.setState({ add_very_fri_to: '' });

        delete this.state.errormsg["add_very_fri_from"];
        delete this.state.errormsg["add_very_fri_to"];
        
      }else{
       this.setState({ very_fri: true }); 
      }
    }

    if(input=="very_sat")
    {
      if(this.state.very_sat == true){
        this.setState({ very_sat: false });

        this.setState({ add_very_sat_from: '' });
        this.setState({ add_very_sat_to: '' });
        
        delete this.state.errormsg["add_very_sat_from"];
        delete this.state.errormsg["add_very_sat_to"];

      }else{
       this.setState({ very_sat: true }); 
      }
    }

  }

  Clear_Same_Day_Fun(){
    this.setState({ add_same_time_from : ""});
    this.setState({ add_same_time_to : ""});
    this.setState({ same_sun : false});
    this.setState({ same_mon : false});
    this.setState({ same_tue : false});
    this.setState({ same_wed : false});
    this.setState({ same_thu : false});
    this.setState({ same_fri : false});
    this.setState({ same_sat : false});

    delete this.state.errormsg["AddShift_Type"];
    delete this.state.errormsg["add_same_time_from"];
    delete this.state.errormsg["add_same_time_to"];
    delete this.state.errormsg["add_same_day"];
    
  }

  Clear_Very_Day_Fun(){
    this.setState({ very_sun : false});
    this.setState({ very_mon : false});
    this.setState({ very_tue : false});
    this.setState({ very_wed : false});
    this.setState({ very_thu : false});
    this.setState({ very_fri : false});
    this.setState({ very_sat : false});

    this.setState({ add_very_sun_from: ''});
    this.setState({ add_very_sun_to:''});
    this.setState({ add_very_mon_from: ''});
    this.setState({ add_very_mon_to:''});
    this.setState({ add_very_tue_from: ''});
    this.setState({ add_very_tue_to:''});
    this.setState({ add_very_wed_from: ''});
    this.setState({ add_very_wed_to:''});
    this.setState({ add_very_thu_from: ''});
    this.setState({ add_very_thu_to:''});
    this.setState({ add_very_fri_from: ''});
    this.setState({ add_very_fri_to:''});
    this.setState({ add_very_sat_from: ''});
    this.setState({ add_very_sat_to:''});

    delete this.state.errormsg["add_very_day"];

    delete this.state.errormsg["add_very_sun_from"];
    delete this.state.errormsg["add_very_sun_to"];

    delete this.state.errormsg["add_very_mon_from"];
    delete this.state.errormsg["add_very_mon_to"];


    delete this.state.errormsg["add_very_tue_from"];
    delete this.state.errormsg["add_very_tue_to"];

    delete this.state.errormsg["add_very_wed_from"];
    delete this.state.errormsg["add_very_wed_to"];

    delete this.state.errormsg["add_very_thu_from"];
    delete this.state.errormsg["add_very_thu_to"];

    delete this.state.errormsg["add_very_fri_from"];
    delete this.state.errormsg["add_very_fri_to"];

    delete this.state.errormsg["add_very_sat_from"];
    delete this.state.errormsg["add_very_sat_to"];
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddEmployeeName: this.props.staffContactID });
    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });
    this.setState({ AddShift_Type: '' });
    this.setState({ AddWork_Hours: '' });
    this.setState({ same_mandatory: '' });

    $("#night_shift").prop('checked', false);
    $("#day_shift").prop('checked', false);

    $("#same_time_each").prop('checked', false);
    $("#very_by_day").prop('checked', false);
    
    this.Clear_Same_Day_Fun();
    this.Clear_Very_Day_Fun();

    
    this.setState({ errormsg: '' });
  }

  ClearRecordFunc (){

    this.setState({ AddEmployeeName: this.props.staffContactID });
    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });
    this.setState({ AddShift_Type: '' });
    this.setState({ AddWork_Hours: '' });
    this.setState({ same_mandatory: '' });

    $("#night_shift").prop('checked', false);
    $("#day_shift").prop('checked', false);

    $("#same_time_each").prop('checked', false);
    $("#very_by_day").prop('checked', false);
    
    this.Clear_Same_Day_Fun();
    this.Clear_Very_Day_Fun();

    
    this.setState({ errormsg: '' });
  }


  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  componentDidMount() {

    /* Role Management */
     console.log('Role Store availability_can');
     /*var getrole = SystemHelpers.GetRole();
     let address_can = getrole.address_can;
     this.setState({ role_address_can: address_can });
     console.log(address_can);*/
     
     console.log(this.props.availability_can);
     let address_can = this.props.availability_can;
     this.setState({ role_availability_can: this.props.availability_can });
    /* Role Management */
    // this.GetCRMRegisterUser(1,100,'');
    // this.GetUserAvailibity();
    
  }

  TabClickOnLoadAvailability = () => e => {
    //debugger;
    e.preventDefault();
    this.GetCRMRegisterUser(1,100,'');
    this.GetUserAvailibity();
  }

  GetUserAvailibityDateWise(AddStartDate,AddShift_Type){
      this.showLoader();
      this.setState({ DateList: [] });
      var AddStartDate=moment(AddStartDate).format('MM/DD/YYYY');

      var AddEndDate= '';
      if(this.state["AddEndDate"] !=''){
        var AddEndDate=moment(this.state["AddEndDate"]).format('MM/DD/YYYY');
      }
      
      
      var url=process.env.API_API_URL+'GetUserAvailibityDateWise'+'?contactId='+this.state.staffContactID+"&startDate="+AddStartDate+"&endDate="+AddEndDate+"&shiftType="+AddShift_Type;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserAvailibityDateWise");
          console.log(data);
          if (data.responseType === "1") {
              
              //his.setState({ ListGrid: data.data });
             // this.setState({ ListGrid: this.rowData(data.data) })
             if(data.data.length > 0){
              this.setState({ DateList: data.data });
             }else{
              this.setState({ CalendarLoad: true });
             }
             
            this.hideLoader();
          }else{
            this.setState({ CalendarLoad: true });
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetUserAvailibity(){
      this.setState({ CalendarLoad: false });
      this.showLoader();
      var url=process.env.API_API_URL+'GetUserAvailibity'+'?contactId='+this.state.staffContactID;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetUserAvailibity");
          console.log(data);
          if (data.responseType === "1") {
              
              //his.setState({ ListGrid: data.data });
             // this.setState({ ListGrid: this.rowData(data.data) })
             if(data.data.length > 0){
              this.GetCalendarAvailabilityList(data.data);
             }else{
              this.setState({ CalendarLoad: true });
             }
             
            this.hideLoader();
          }else{
            this.setState({ CalendarLoad: true });
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetCalendarAvailabilityList(list){

    console.log('GetCalendarAvailabilityList 1');
    console.log(new Date(now() + 148000000));
    console.log(list[0].startDate);
    console.log(moment(new Date(list[0].startDate)));

    let list_push =[];
    for (var i = 0; i<list.length; i++) {
      let temp_list={};
      var fromtime= moment(list[i].startDate).format(process.env.TIME_FORMAT);
      var totime= moment(list[i].endDate).format(process.env.TIME_FORMAT);

      if(list[i].shift == 'Day Shift'){
        var title= fromtime+" - "+totime+' AVAILABLE';
        //var title= fromtime+" - "+'<br /> AVAILABLE';
        var color = 'pk-bg-pink';
      }else{
        var title= 'NIGHT SHIFT AVAILABLE';
        var color = 'pk-bg-skyblue';
      }
      
      temp_list["title"] = title;
      temp_list["start"] = new Date(list[i].startDate);
      temp_list["end"] = new Date(list[i].endDate);
      temp_list["className"] = color;
      temp_list["contactId"] = list[i].contactId;
      temp_list["shift"] = list[i].shift;
      temp_list["workHours"] = list[i].workHours;
      temp_list["aid"] = list[i].id;
      temp_list["startdatetime"] = new Date(list[i].startDate);
      temp_list["enddatetime"] = new Date(list[i].endDate);
      list_push.push(temp_list);
    }


    // var defaultEvents =  [{
    //         title: 'Event Name pk',
    //         start: new Date(list[0].startDate),
    //         className: 'bg-purple'
    //     },
    //     {
    //         title: 'Test Event 1',
    //         start: new Date(now()),
    //         end: new Date(now()),
    //         className: 'bg-success'
    //     },
    //     {
    //         title: 'Test Event 2',
    //         start: new Date(now() + 168000000),
    //         className: 'bg-info'
    //     },
    //     {
    //         title: 'Test Event 3',
    //         start: new Date(now() + 338000000),
    //         className: 'bg-primary'
    //     }];
        console.log(list_push);

        this.setState({ defaultEvents : list_push });
        this.setState({ CalendarLoad: true });
        return false;
  }


  GetCRMRegisterUser(currentPage,pageSize,searchText){

      this.setState({ CRMRegisterUser : [] });

      let bodyarray = {};
      bodyarray["currentPage"] = 1;
      bodyarray["nextPage"] = false;
      bodyarray["pageSize"] = 5;
      bodyarray["previousPage"] = false;
      bodyarray["totalCount"] = 0;
      bodyarray["totalPages"] = 0;
      
      

      //if(this.props.employees_can_delete == true){
        /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.employees_can.employees_can_delete;
      //let canViewall = getrole.employees_can.employees_can_viewall
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
      console.log('employee getrole');
      console.log(getrole.employees_can.employees_can_viewall);
      console.log(getrole);
        //let canDelete = this.state.role_employees_can.employees_can_delete;
      //}


      //this.setState({ currentPage: currentPage });
      //this.setState({ pageSize: pageSize });

      var sort_Column = '';
      var Sort_Type = false;
      var roleId = '';
      var locationID = localStorage.getItem("primaryLocationId");

      var IsSortingEnabled = true;
      

      var pass_url = 'GetCRMRegisterUser?pageNumber='+currentPage+'&pageSize='+pageSize+'&searchText='+searchText+'&canDelete='+canDelete+'&sortColumn='+sort_Column+'&SortType='+Sort_Type+'&IsSortingEnabled='+IsSortingEnabled+'&roleId='+roleId+'&rolePriorityId='+hierarchyId+"&locationId="+locationID+"&contactId="+localStorage.getItem("contactId");


      this.showLoader();
      var url=process.env.API_API_URL+pass_url;
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetCRMRegisterUser");
          console.log(data);
          if (data.responseType === "1") {
              this.setState({ CRMRegisterUser: data.data });
              //this.setState({ pagingData: data.pagingData });

              //this.setState({ IsSortingEnabled: true });

              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  AddModalOpen = () => e => {

    e.preventDefault();
    this.ClearRecordFunc();
    this.setState({ submit_type : '' });
    this.setState({ DeleteList : {} });
    this.setState({ DateList : [] });

    $('#Availability_Add_modal').modal('show');
  }

  AddRecord = () => e => {
    //alert();
      //debugger;
      

      e.preventDefault();

      

      let step1Errors = {};
      
      console.log(this.state);
      if (this.state["AddEmployeeName"] == '' || this.state["AddEmployeeName"] == null) {
        step1Errors["AddEmployeeName"] = "Employee Name is mandatory";
      }

      if (this.state["AddStartDate"] == '') {
        step1Errors["AddStartDate"] = "Start Date is mandatory";
      }

      // if (this.state["AddEndDate"] == '') {
      //   step1Errors["AddEndDate"] = "End Date is mandatory";
      // }

      if (this.state["AddShift_Type"] == '' && this.state["AddShift_Type"] != false) {
        step1Errors["AddShift_Type"] = "Shift Type is mandatory";
      }

      if (this.state["AddWork_Hours"] == '') {
        step1Errors["AddWork_Hours"] = "Work Hours is mandatory";
      }


      if(this.state["AddWork_Hours"] == "same_time_each"){
        
        if(this.state["AddShift_Type"] == "true" || this.state["AddShift_Type"] == true){
          if (this.state["add_same_time_from"] == '') {
            step1Errors["add_same_time_from"] = "Time from is mandatory";
          }

          if (this.state["add_same_time_to"] == '') {
            step1Errors["add_same_time_to"] = "Time to is mandatory";
          }
        }
        

        if (this.state["same_sun"] == false && this.state["same_mon"] == false && this.state["same_tue"] == false && this.state["same_wed"] == false && this.state["same_thu"] == false && this.state["same_fri"] == false && this.state["same_sat"] == false ) {
          step1Errors["add_same_day"] = "Day is mandatory";
        }
      }

      if(this.state["AddWork_Hours"] == "very_by_day"){
        if(this.state["very_sun"] == true){
          if (this.state["add_very_sun_from"] == '') {
            step1Errors["add_very_sun_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_sun_to"] == '') {
            step1Errors["add_very_sun_to"] = "Time to is mandatory";
          }
        }

        if(this.state["very_mon"] == true){
          if (this.state["add_very_mon_from"] == '') {
            step1Errors["add_very_mon_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_mon_to"] == '') {
            step1Errors["add_very_mon_to"] = "Time to is mandatory";
          }
        }


        if(this.state["very_tue"] == true){
          if (this.state["add_very_tue_from"] == '') {
            step1Errors["add_very_tue_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_tue_to"] == '') {
            step1Errors["add_very_tue_to"] = "Time to is mandatory";
          }
        }


        if(this.state["very_wed"] == true){
          if (this.state["add_very_wed_from"] == '') {
            step1Errors["add_very_wed_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_wed_to"] == '') {
            step1Errors["add_very_wed_to"] = "Time to is mandatory";
          }
        }

        if(this.state["very_thu"] == true){
          if (this.state["add_very_thu_from"] == '') {
            step1Errors["add_very_thu_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_thu_to"] == '') {
            step1Errors["add_very_thu_to"] = "Time to is mandatory";
          }
        }

        if(this.state["very_fri"] == true){
          if (this.state["add_very_fri_from"] == '') {
            step1Errors["add_very_fri_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_fri_to"] == '') {
            step1Errors["add_very_fri_to"] = "Time to is mandatory";
          }
        }

        if(this.state["very_sat"] == true){
          if (this.state["add_very_sat_from"] == '') {
            step1Errors["add_very_sat_from"] = "Time from is mandatory";
          }

          if (this.state["add_very_sat_to"] == '') {
            step1Errors["add_very_sat_to"] = "Time to is mandatory";
          }
        }

        if (this.state["very_sun"] == false && this.state["very_mon"] == false && this.state["very_tue"] == false && this.state["very_wed"] == false && this.state["very_thu"] == false && this.state["very_fri"] == false && this.state["very_sat"] == false ) {
          step1Errors["add_very_day"] = "Day is mandatory";
        }
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      let availability_day = [];
      if(this.state["AddWork_Hours"] == "same_time_each"){

        // this.state["add_same_time_from"] this.state["add_same_time_to"]
        var add_same_time_from =  this.state["add_same_time_from"];
        var add_same_time_to = this.state["add_same_time_to"];

        if((this.state["AddShift_Type"] == "false" || this.state["AddShift_Type"] == false) && (add_same_time_from == '' || add_same_time_to == '')){
          var add_same_time_from =  '20:00';
          var add_same_time_to = '23:59';
        }

        if(this.state["same_sun"] == true){
          let ArrayJson = {
            day: 'Sunday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["same_mon"] == true){
          let ArrayJson = {
            day: 'Monday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["same_tue"] == true){
          let ArrayJson = {
            day: 'Tuesday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["same_wed"] == true){
          let ArrayJson = {
            day: 'Wednesday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["same_thu"] == true){
          let ArrayJson = {
            day: 'Thursday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["same_fri"] == true){
          let ArrayJson = {
            day: 'Friday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["same_sat"] == true){
          let ArrayJson = {
            day: 'Saturday',
            timefrom: add_same_time_from,
            timeto: add_same_time_to
          };

          availability_day.push(ArrayJson);
        }
      }

      if(this.state["AddWork_Hours"] == "very_by_day"){

        if(this.state["very_sun"] == true){
          let ArrayJson = {
            day: 'Sunday',
            timefrom: this.state["add_very_sun_from"],
            timeto: this.state["add_very_sun_to"]
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["very_mon"] == true){
          let ArrayJson = {
            day: 'Monday',
            timefrom: this.state["add_very_mon_from"],
            timeto: this.state["add_very_mon_to"]
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["very_tue"] == true){
          let ArrayJson = {
            day: 'Tuesday',
            timefrom: this.state["add_very_tue_from"],
            timeto: this.state["add_very_tue_to"]
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["very_wed"] == true){
          let ArrayJson = {
            day: 'Wednesday',
            timefrom: this.state["add_very_wed_from"],
            timeto: this.state["add_very_wed_to"]
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["very_thu"] == true){
          let ArrayJson = {
            day: 'Thursday',
            timefrom: this.state["add_very_thu_from"],
            timeto: this.state["add_very_thu_to"]
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["very_fri"] == true){
          let ArrayJson = {
            day: 'Friday',
            timefrom: this.state["add_very_fri_from"],
            timeto: this.state["add_very_fri_to"]
          };

          availability_day.push(ArrayJson);
        }

        if(this.state["very_sat"] == true){
          let ArrayJson = {
            day: 'Saturday',
            timefrom: this.state["add_very_sat_from"],
            timeto: this.state["add_very_sat_to"]
          };

          availability_day.push(ArrayJson);
        }
      }

      var date_array =this.addDays(availability_day);
      //console.log(availability_day);
      // console.log(date_array);
      // return false;

      if(date_array.length == 0){
        if(this.state["AddWork_Hours"] == "same_time_each"){
          step1Errors["add_same_day"] = "select valid weekday.";
        }

        if(this.state["AddWork_Hours"] == "very_by_day"){
          step1Errors["add_very_day"] = "select valid weekday.";
        }
      }
      

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
          return false;
      }

      //return false;
      // console.log('date_array');
      // console.log(date_array);
      // console.log(date_array.length);

      if(this.state["AddShift_Type"] == "false" || this.state["AddShift_Type"] == false){
        var AddShift_Type ='Night Shift';
      }else{
        var AddShift_Type ='Day Shift';
      }


      let AvailibityList = [];
      var DateList = this.state.DateList;

      if(this.state["AddEndDate"] =='' && this.state.submit_type == 'update' && date_array.length == 1){
          let datedetails = {};
          datedetails.Date = date_array[0].Date;
          datedetails.TimeFrom = date_array[0].TimeFrom;
          datedetails.TimeTo = date_array[0].TimeTo;
          datedetails.id = this.state.DeleteList.aid;
          AvailibityList.push(datedetails);
      }else{
        for (var i = 0; i < date_array.length; i++) {
          let datedetails = {};
          //console.log('date_array');
          //console.log(date_array[i]);
          datedetails.Date = date_array[i].Date;
          datedetails.TimeFrom = date_array[i].TimeFrom;
          datedetails.TimeTo = date_array[i].TimeTo;
          
          var aid = '';
          for (var z = 0; z < DateList.length; z++) {
              var startDate = moment(DateList[z].startDate).format('MM/DD/YYYY')
              if((DateList[z].shift == AddShift_Type  && startDate == date_array[i].Date )){
                aid= DateList[z].id;
              }
          }
          datedetails.id = aid;
          AvailibityList.push(datedetails);
          
        }
      }
      

      //console.log(this.state.DateList);
      
      //console.log(AvailibityList);
      //return false;

      var AddStartDate=moment(this.state["AddStartDate"]).format('MM/DD/YYYY');
      var AddEndDate=moment(this.state["AddEndDate"]).format('MM/DD/YYYY');
      
      if(this.state["AddWork_Hours"] == "same_time_each"){
        var WorkHours = 'S';
      }else{
        var WorkHours = 1;
      }

      

      this.showLoader();

      let bodyarray = {};
      bodyarray["ContactId"] = this.state["AddEmployeeName"];
      bodyarray["LoggedInUserId"] = localStorage.getItem('contactId');
      bodyarray["StartDate"] = AddStartDate;
      bodyarray["EndDate"] = AddEndDate;
      bodyarray["WorkHours"] = WorkHours;
      bodyarray["Shift"] = AddShift_Type;
      bodyarray["UserAvailibilityTimeModel"] = AvailibityList
      
      console.log(bodyarray);
      //return false;

      var url=process.env.API_API_URL+'PostUserAvailibity';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateHolidayList");
          console.log(data);
          //console.log(responseJson);
          // debugger;
          if (data.responseType === "1") {

              SystemHelpers.ToastSuccess(data.responseMessge);  
              this.GetUserAvailibity();
              $( ".close" ).trigger( "click" ); 
              
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              SystemHelpers.ToastError(data.responseMessge);  
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
      
  }

  DeleteRecord = () => e => {
      e.preventDefault();

      var isdelete = '';
      if(this.state.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }

      this.showLoader();
      var DeleteList = this.state.DeleteList.aid;
      var url=process.env.API_API_URL+'DeleteUserAvailibilty?id='+DeleteList+'&isDelete='+isdelete+'&userName='+localStorage.getItem('fullName');
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        //body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DeleteLocation");
          console.log(data);
          //console.log(data.data.userRole);
          // debugger;
          if (data.responseType === "1") {
              // Profile & Contact
              SystemHelpers.ToastSuccess(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.GetUserAvailibity();
              this.hideLoader();
          }else if (data.responseType == "2" || data.responseType == "3") {
              SystemHelpers.ToastError(data.responseMessge);
              $( ".cancel-btn" ).trigger( "click" );
              this.hideLoader();
          }else{
                if(data.message == 'Authorization has been denied for this request.'){
                  SystemHelpers.SessionOut();
                  this.props.history.push("/login");
                }else{
                  SystemHelpers.ToastError(data.message);
                }
                this.hideLoader();
                $( ".cancel-btn" ).trigger( "click" );
          }
          
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  ModelOpen = (modelname) => e => {
    e.preventDefault();

    if(modelname == 'delete'){
      $('#Availability_Update_Delete_modal').modal('hide');
      $('#Availability_Delete_modal').modal('show');
      this.setState({ submit_type : 'delete' });
    }else{

      this.ClearRecordFunc();

      $('#Availability_Update_Delete_modal').modal('hide');
      $('#Availability_Add_modal').modal('show');

      this.setState({ submit_type : 'update' });
      
      var extendedProps = this.state.DeleteList;

      var shift = extendedProps.shift;
      if(shift == 'Day Shift'){
        $( "#day_shift" ).trigger( "click" );
        this.setState({ AddShift_Type : true });
      }else{
        $( "#night_shift" ).trigger( "click" );
        this.setState({ AddShift_Type : false });
      }

      var workHours = extendedProps.workHours;
      var temp_day = moment(extendedProps.startdatetime).format('dddd');
      if(workHours == 'Vary By day'){
        $( "#very_by_day" ).trigger( "click" );
        this.setState({ AddWork_Hours : 'very_by_day' });
        
        //alert(temp_day);
        if(temp_day =="Sunday")
        {
          this.setState({ very_sun: true });
          this.setState({ add_very_sun_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_sun_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

        if(temp_day =="Monday")
        {
          this.setState({ very_mon: true });
          this.setState({ add_very_mon_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_mon_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

        if(temp_day =="Tuesday")
        {
          this.setState({ very_tue: true });
          this.setState({ add_very_tue_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_tue_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

        if(temp_day =="Wednesday")
        {
          this.setState({ very_wed: true });
          this.setState({ add_very_wed_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_wed_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

        if(temp_day =="Thursday")
        {
          this.setState({ very_thu: true });
          this.setState({ add_very_thu_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_thu_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

        if(temp_day =="Friday")
        {
          this.setState({ very_fri: true });
          this.setState({ add_very_fri_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_fri_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

        if(temp_day =="Saturday")
        {
          this.setState({ very_sat: true });
          this.setState({ add_very_sat_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
          this.setState({ add_very_sat_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        }

      }else{
        $( "#same_time_each" ).trigger( "click" );
        this.setState({ AddWork_Hours : 'same_time_each' });

        if(temp_day =="Sunday")
        {
          this.setState({ same_sun: true });
        }else if(temp_day =="Monday")
        {
          this.setState({ same_mon: true });
        }else if(temp_day =="Tuesday")
        {
          this.setState({ same_tue: true });
        }else if(temp_day =="Wednesday")
        {
          this.setState({ same_wed: true });
        }else if(temp_day =="Thursday")
        {
          this.setState({ same_thu: true });
        }else if(temp_day =="Friday")
        {
          this.setState({ same_fri: true });
        }else if(temp_day =="Saturday")
        {
          this.setState({ same_sat: true });
        }

        this.setState({ add_same_time_from: moment(extendedProps.startdatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        this.setState({ add_same_time_to: moment(extendedProps.enddatetime,'MM/DD/YYYY h:mm A').format('HH:mm') });
        
      }

      var AddStartDate = moment(extendedProps.startdatetime);
      
      this.setState({  AddStartDate: AddStartDate });

      if(shift == 'Day Shift'){
        this.GetUserAvailibityDateWise(AddStartDate,true);
      }else{
        this.GetUserAvailibityDateWise(AddStartDate,false);
      }
      
    }
  }

  addDays = (availability_day) => {

    console.log(availability_day);

    //console.log(date_array);
    //return false;
    var today = moment();
    let datesCollection = [];
    console.log(this.state["AddStartDate"]);
    console.log(this.state["AddEndDate"]);
    var startdate = moment(this.state["AddStartDate"], "DD/MM/YYYY");

    if(this.state["AddEndDate"] != ''){
      var enddate = moment(this.state["AddEndDate"], "DD/MM/YYYY");
      var diff_day_count = enddate.diff(startdate, 'days') // 1
    }else{
      var diff_day_count = 1;
    }
    

    console.log(diff_day_count);
    var z = 0;
    for (var i = 0; i <= diff_day_count; i++) {
      let datedetails = {};
      var today = moment();
      var dateuse = moment(this.state["AddStartDate"]).add(z, 'days');

  
      var temp_date = moment(dateuse).format('MM/DD/YYYY');
      var temp_day = moment(dateuse).format('dddd');

      for (var j = 0; j<availability_day.length; j++) {
        if(availability_day[j].day == temp_day){
          var startDate = moment(dateuse).format('MM/DD/YYYY');
          var endDate = moment(dateuse).format('MM/DD/YYYY');

          var StartTime = moment(availability_day[j].timefrom, 'HH:mm').format('HH');
          var EndTime = moment(availability_day[j].timeto, 'HH:mm').format('HH');
          if(StartTime > EndTime){
            var endDate = moment(dateuse).add(1, 'day').format('MM/DD/YYYY');
          }

          datedetails.Date = moment(dateuse).format('MM/DD/YYYY');
          datedetails.TimeFrom = startDate+" "+moment(availability_day[j].timefrom, 'HH:mm').format('h:mm A');
          datedetails.TimeTo = endDate+" "+moment(availability_day[j].timeto, 'HH:mm').format('h:mm A');

          datesCollection.push(datedetails);
        }
      }
      
      z++;
    }

    return datesCollection
  }

  
  handleNewSelect = (selectInfo) => {
    console.log('selectInfo');
    console.log(selectInfo);
    this.setState({ submit_type : '' });
    $( "#availability_add_modal" ).trigger( "click" );
    this.setState({ AddStartDate: moment(selectInfo.start) });


    // this.setState({
    //   isnewevent : true,
    //   addneweventobj : selectInfo
    // }) 
    
  }

  handleDeleteEventClick = (clickInfo) => {    

    console.log("clickInfo");
    console.log(clickInfo);
    // console.log(clickInfo.event.extendedProps);
    this.setState({ submit_type : '' });
    $('#Availability_Update_Delete_modal').modal('show');
    this.setState({ DeleteList : clickInfo.event.extendedProps });
    
    
    // this.setState({
    //   iseditdelete : true,
    //   event_title : clickInfo.event.title,
    //   calenderevent : clickInfo.event
    // })    
  }



  render() {
     const data = {
              columns: this.state.header_data,
              rows: this.state.ListGrid
            };
      return (

        
            <div>
              {/* Page Content */}
              {/* Toast & Loder method use */}
            
               {(this.state.loading) ? <Loader /> : null} 
              {/* Toast & Loder method use */}
              {/* Page Header */}
              <div className="page-header">
                <div className="row align-items-center">
                  <div className="col">
                    <h3 className="page-title">Availability</h3>
                    <ul className="breadcrumb">
                      <li className="breadcrumb-item"><a href="/staff-profile">Profile</a></li>
                      <li className="breadcrumb-item active">Availability</li>
                    </ul>
                  </div>
                  <div className="row">
                    <button className="btn btn-primary submit-btn pk-profiletab-refreshbtn-hide" id="TabClickOnLoadAvailability" onClick={this.TabClickOnLoadAvailability()}>Refresh</button>
                  </div>
                  <div className="col-auto float-right ml-auto">
                    {this.state.role_availability_can.availability_can_create == true ?
                     <a href="#" className="btn add-btn"  onClick={this.AddModalOpen()}><i className="fa fa-plus" /> Add Availability</a>
                     : <h3 className="card-title"> <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                    }
                  </div>
                </div>
              </div>
              {/* /Page Header */}
              <div className="row">
                <div className="col-lg-12">
                  <div className="card mb-0">
                    <div className="card-body">
                      <div className="row">
                        <div className="col-md-12">
                          {/* Calendar */}
                          {/* <div id="calendar" /> */}
                          {this.state.CalendarLoad == true ? 
                            <FullCalendar
                                plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                                headerToolbar={{
                                  left: 'prev,next today',
                                  center: 'title',
                                  right: 'dayGridMonth,timeGridWeek,timeGridDay'
                                }}
                                data-target="#add_event"
                                initialView='dayGridMonth'
                                displayEventTime= {false}
                                editable={true}
                                selectable={true}
                                selectMirror={true}
                                dayMaxEvents={false}
                                weekends={this.state.weekendsVisible}
                                initialEvents={this.state.defaultEvents} // alternatively, use the `events` setting to fetch from a feed
                                select={this.handleNewSelect}
                                // eventContent={renderEventContent} // custom render function
                                eventClick={clickInfo=>this.handleDeleteEventClick(clickInfo)}
                                // eventsSet={this.handleEvents} // called after events are initialized/added/changed/removed
                                /* you can update a remote database when these fire:
                                eventAdd={function(){}}
                                eventChange={function(){}}
                                eventRemove={function(){}}
                                */
                              />
                              : null
                            }
                          {/* /Calendar */}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            
            {/* /Page Content */}
            
            
           
            
            

            


            {/* ****************** Availability Departments Tab Modals ******************* */}
            {/* Availability Modal */}
            <div id="Availability_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Availability</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Employee Name <span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddEmployeeName} onChange={this.handleChange('AddEmployeeName')} disabled>
                                  <option value="">-</option>
                                  <option value={this.state.staffContactID} selected>{this.state.all_data.firstName} {this.state.all_data.firstName} {this.state.all_data.lastName}</option>
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddEmployeeName"]}</span>
                              </div>
                            </div>
                            
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>Start Date <span className="text-danger">*</span></label>
                                <Datetime
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                onChange={this.handleAddStartDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddStartDate} onChange={this.handleChange('AddStartDate')} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                              </div>
                            </div>
                            <div className="col-md-4">
                              <div className="form-group">
                                <label>End Date </label>
                                <Datetime
                                isValidDate={this.validationAddEndDate}
                                inputProps={{readOnly: true}}
                                closeOnTab={true}
                                input={true}
                                value={(this.state.AddEndDate) ? this.state.AddEndDate : ''}
                                onChange={this.handleAddEndDate}
                                dateFormat={process.env.DATE_FORMAT}
                                timeFormat={false}
                                renderInput={(props) => {
                                   return <input {...props} value={(this.state.AddEndDate) ? props.value : ''} />
                                }}
                              />
                                {/*<input className="form-control" type="date" value={this.state.AddEndDate} onChange={this.handleChange('AddEndDate')} min={moment().format(this.state.AddStartDate,"YYYY-MM-DD")} />*/}
                                <span className="form-text error-font-color">{this.state.errormsg["AddEndDate"]}</span>
                              </div>
                            </div>

                            <div className="col-md-12">
                              <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Shift Type <span className="text-danger">*</span></label>
                                <div class="col-lg-9 pk-availability-margin-top">
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="shift_type" value="false" id="night_shift" onChange={this.handleChange('AddShift_Type')}/>
                                    <label class="form-check-label" for="shift_type">Night Shift</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="shift_type" value="true" id="day_shift"  onChange={this.handleChange('AddShift_Type')}/>
                                    <label class="form-check-label" for="shift_type">Day Shift</label>
                                  </div>

                                </div>
                                <span className="form-text error-font-color pk-spna">{this.state.errormsg["AddShift_Type"]}</span>
                              </div>
                              
                            </div>

                            <div className="col-md-12">
                              <div class="form-group row">
                                <label class="col-lg-3 col-form-label">Work hours <span className="text-danger">*</span></label>
                                <div class="col-lg-9 pk-availability-margin-top">
                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="work_hours" id="same_time_each" value="same_time_each" onChange={this.handleChange('AddWork_Hours')}/>
                                    <label class="form-check-label" for="work_hours">Are the same each day</label>
                                  </div>

                                  <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="work_hours" id="very_by_day" value="very_by_day"  onChange={this.handleChange('AddWork_Hours')}/>
                                    <label class="form-check-label" for="work_hours">Vary by day</label>
                                  </div>

                                </div>
                                <span className="form-text error-font-color pk-spna">{this.state.errormsg["AddWork_Hours"]}</span>
                              </div>
                              
                            {this.state.AddWork_Hours == 'same_time_each' ?
                              <div class="" id="same_time_each">
                                <form>
                                  <div class="card pk-card-border">
                                    <div class="card-body">
                                      <h3 className="modal-title pk-font-availability">Same time each day</h3>
                                      <br/>
                                      <div class="row">
                                        <div class="col-md-12">
                                          
                                            <div class="form-group row">
                                              <label class="col-form-label col-md-2">Time from {this.state.same_mandatory == true ? <span className="text-danger">*</span> : null } </label>
                                              <div class="col-md-4">
                                                <input class="form-control" type="time" value={this.state.add_same_time_from} onChange={this.handleChange('add_same_time_from')} />
                                                <span className="form-text error-font-color">{this.state.errormsg["add_same_time_from"]}</span>
                                              </div>

                                              <label class="col-form-label col-md-2">Time to {this.state.same_mandatory == true ? <span className="text-danger">*</span> : null } </label>
                                              <div class="col-md-4">
                                                <input class="form-control" type="time" value={this.state.add_same_time_to} onChange={this.handleChange('add_same_time_to')} />
                                                <span className="form-text error-font-color">{this.state.errormsg["add_same_time_to"]}</span>
                                              </div> 
                                            </div>

                                          <div class="form-group row">
                                            <label class="col-form-label col-md-2"></label>
                                            <div class="col-md-5">
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_sun" value="true" onChange={this.handleChange('same_sun')} checked={this.state.same_sun == true ? "true" : null} /> Sunday</label>
                                              </div>
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_mon" value="true" onChange={this.handleChange('same_mon')} checked={this.state.same_mon == true ? "true" : null} /> Monday</label>
                                              </div>
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_tue" value="true" onChange={this.handleChange('same_tue')} checked={this.state.same_tue == true ? "true" : null} /> Tuesday</label>
                                              </div>
                                            </div>
                                            <div class="col-md-5">
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_wed" value="true" onChange={this.handleChange('same_wed')} checked={this.state.same_wed == true ? "true" : null} /> Wednesday</label>
                                              </div>
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_thu" value="true" onChange={this.handleChange('same_thu')} checked={this.state.same_thu == true ? "true" : null} /> Thursday</label>
                                              </div>
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_fri" value="true" onChange={this.handleChange('same_fri')} checked={this.state.same_fri == true ? "true" : null} /> Friday</label>
                                              </div>
                                              <div class="checkbox">
                                                <label><input type="checkbox" name="same_sat" value="true" onChange={this.handleChange('same_sat')} checked={this.state.same_sat == true ? "true" : null} /> Saturday</label>
                                              </div>
                                            </div>
                                            <label class="col-form-label col-md-2"></label>
                                            <div class="col-md-5">
                                              <span className="form-text error-font-color">{this.state.errormsg["add_same_day"]}</span>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              : null
                            }

                            {this.state.AddWork_Hours == 'very_by_day' ?
                              <div class="" id="very_by_day">
                                
                                  <div class="card pk-card-border">
                                    <div class="card-body">
                                      <h3 className="modal-title pk-font-availability">Vary by day</h3>
                                      <br/>
                                      

                                      <div class="row">
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <label className="d-block pk-label-availability pk-color-blue">Weekdays</label>
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" id="very_sun" name="very_sun" value="true" onChange={this.handleChange('very_sun')} checked={this.state.very_sun == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Sunday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <label className="pk-label-availability pk-color-blue">Time from <span className="text-danger">*</span></label>
                                            <input class="form-control" type="time" value={this.state.add_very_sun_from} onChange={this.handleChange('add_very_sun_from')}  disabled={this.state.very_sun == false ? "disabled" : null}  />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_sun_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <label className="pk-label-availability pk-color-blue">Time to <span className="text-danger">*</span></label>
                                            <input class="form-control" type="time" value={this.state.add_very_sun_to} onChange={this.handleChange('add_very_sun_to')} disabled={this.state.very_sun == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_sun_to"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" name="very_mon" value="true" onChange={this.handleChange('very_mon')} checked={this.state.very_mon == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Monday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_mon_from} onChange={this.handleChange('add_very_mon_from')}  disabled={this.state.very_mon == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_mon_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_mon_to} onChange={this.handleChange('add_very_mon_to')} disabled={this.state.very_mon == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_mon_to"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" name="very_tue" value="true" onChange={this.handleChange('very_tue')} checked={this.state.very_tue == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Tuesday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_tue_from} onChange={this.handleChange('add_very_tue_from')}  disabled={this.state.very_tue == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_tue_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_tue_to} onChange={this.handleChange('add_very_tue_to')} disabled={this.state.very_tue == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_tue_to"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" name="very_wed" value="true" onChange={this.handleChange('very_wed')} checked={this.state.very_wed == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Wednesday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_wed_from} onChange={this.handleChange('add_very_wed_from')}  disabled={this.state.very_wed == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_wed_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_wed_to} onChange={this.handleChange('add_very_wed_to')} disabled={this.state.very_wed == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_wed_to"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" name="very_thu" value="true" onChange={this.handleChange('very_thu')} checked={this.state.very_thu == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Thursday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_thu_from} onChange={this.handleChange('add_very_thu_from')}  disabled={this.state.very_thu == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_thu_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_thu_to} onChange={this.handleChange('add_very_thu_to')} disabled={this.state.very_thu == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_thu_to"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" name="very_fri" value="true" onChange={this.handleChange('very_fri')} checked={this.state.very_fri == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Friday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_fri_from} onChange={this.handleChange('add_very_fri_from')}  disabled={this.state.very_fri == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_fri_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_fri_to} onChange={this.handleChange('add_very_fri_to')} disabled={this.state.very_fri == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_fri_to"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <div class="form-check form-check-inline pk-checkbox-availability">
                                              <input type="checkbox" class="form-check-input" name="very_sat" value="true" onChange={this.handleChange('very_sat')} checked={this.state.very_sat == true ? "true" : null} />
                                              <label class="form-check-label pk-label-checkbox-availability" for="very_week">Saturday</label>
                                            </div>
                                          </div>
                                        </div>
                            
                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_sat_from} onChange={this.handleChange('add_very_sat_from')}  disabled={this.state.very_sat == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_sat_from"]}</span>
                                          </div>
                                        </div>

                                        <div className="col-md-4">
                                          <div className="form-group">
                                            <input class="form-control" type="time" value={this.state.add_very_sat_to} onChange={this.handleChange('add_very_sat_to')} disabled={this.state.very_sat == false ? "disabled" : null} />
                                            <span className="form-text error-font-color">{this.state.errormsg["add_very_sat_to"]}</span>
                                          </div>
                                        </div>
                                      </div>

                                      <span className="form-text error-font-color">{this.state.errormsg["add_very_day"]}</span>
                                      
                                    </div>
                                  </div>
                                
                              </div>
                              : null
                            }

                              {/*<div class="form-group">
                                <label>Work hours</label>
                                <div class="col-md-10">
                                  <div class="radio">
                                    <label><input type="radio" name="radio" /> Are the same each day</label>
                                  </div>
                                  <div class="radio">
                                    <label><input type="radio" name="radio" /> Very by day</label>
                                  </div>
                                </div>
                              </div>*/}

                            </div>
                          </div>
                          
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    
                  </div>
                </div>
              </div>
            </div>
            {/* //Availability Modal */}

            

          {/* Update or Delete Availability  Modal */}
            <div className="modal custom-modal fade" id="Availability_Update_Delete_modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Availability</h3>
                      <p>Are you sure you want to mark Availability as Update or Delete ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.ModelOpen('delete')} className="btn pk-btn-danger continue-btn">Delete</a>
                        </div>
                        <div className="col-6">
                          <a onClick={this.ModelOpen('update')} className="btn pk-btn-success continue-btn">Update</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* /Update or Delete Availability Modal */}

          {/* Delete Availability  Modal */}
            <div className="modal custom-modal fade" id="Availability_Delete_modal" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Availability</h3>
                      <p>Are you sure you want to Delete ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">Delete</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          {/* /Delete Availability Modal */}


        {/* /****************** Locations Departments Tab Modals ******************* */}
            
        </div>   
      );
   }
}

export default Availability;
