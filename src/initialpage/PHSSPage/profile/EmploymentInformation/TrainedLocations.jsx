/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";

import moment from 'moment';
import Loader from '../../../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../../../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';
//table


class TrainedLocations extends Component {
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        user_role: [],
        ListGrid : [],
        staffContactID:this.props.staffContactID,
        trainedWorkList: [],
        locationList:[],
        TraininType:[{"id": 'Yes',"name": "Yes"},
            {"id": 'No',"name": "No"}
        ],
        // Edit Model
        EdittrainedWork :  '',
        Editlocation :  '',
        EdittrainningType :  '',
        EditdateCompleted :  '',
        EditdateLastWork :  '',
        EditnoOfHoursWork :  '',
        EditID : '',
        trainedLocationId:'',
        // Edit Model

        // Add Model
        AddtrainedWork :  '',
        Addlocation :  '',
        AddtrainningType :  '',
        AdddateCompleted :  '',
        AdddateLastWork :  '',
        AddnoOfHoursWork :  '',
        // Add Model
        role_course_training_can: {},

        isDelete : false,

        header_data : [],
        staffContactFullname : localStorage.getItem('fullName')
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value.replace(/^\s+/g, '') });
    //console.log(input);
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
    
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  

  componentDidMount() {
    this.setState({ Addlocation: this.props.primaryLocationId });
    console.log('didmount 123');
    console.log(this.props.primaryLocationId);
    /* Role Management */
     console.log('Role Store course_training_can');
     /*var getrole = SystemHelpers.GetRole();
     let course_training_can = getrole.course_training_can;
     this.setState({ role_course_training_can: course_training_can });
     console.log(course_training_can);*/

     console.log(this.props.course_training_can);
    let course_training_can = this.props.course_training_can;
    this.setState({ role_course_training_can: this.props.course_training_can });
    /* Role Management */

    this.GetUserTrainedLocations();
    this.GetProfile();

    // Delete Permison
    if(course_training_can.course_training_can_delete == true){
      var columns = [
                {
                  label: 'Trained/Worked',
                  field: 'trainedWork',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Location',
                  field: 'location',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Training Type',
                  field: 'trainningType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Date Completed',
                  field: 'dateCompleted',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Date Last Worked',
                  field: 'dateLastWork',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: '# of Hrs Worked',
                  field: 'noOfHoursWork',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Status',
                  field: 'status',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

      this.setState({ header_data: columns });
    }else{
      var columns = [
                {
                  label: 'Trained/Worked',
                  field: 'trainedWork',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Location',
                  field: 'location',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Training Type',
                  field: 'trainningType',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Date Completed',
                  field: 'dateCompleted',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Date Last Worked',
                  field: 'dateLastWork',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: '# of Hrs Worked',
                  field: 'noOfHoursWork',
                  sort: 'asc',
                  width: 150
                },
                {
                  label: 'Action',
                  field: 'action',
                  width: 270
                }
              ];

      this.setState({ header_data: columns });
    }
    // Delete Permison
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_course_training_can.course_training_can_update == true || this.state.role_course_training_can.course_training_can_delete == true){
      let Edit_push = [];
      if(this.state.role_course_training_can.course_training_can_update == true){
        Edit_push.push(
          <a href="#" onClick={this.EditRecord(record)} data-toggle="modal" data-target="#EmploymentInformation_trained_locations_Edit_modal" className="dropdown-item"><i className="fa fa-pencil m-r-5" /> Edit</a>
        );
      }
      let Delete_push = [];
      if(this.state.role_course_training_can.course_training_can_delete == true){
        if(record.isDelete == false)
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_trained_locations"><i className="fa fa-trash-o m-r-5" /> Inactive</a>
          );
        }
        else
        {
          Delete_push.push(
            <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_trained_locations"><i className="fa fa-trash-o m-r-5" /> Active</a>
          );
        }
      }
      
      return_push.push(
        <div className="dropdown dropdown-action">
          <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
          <div className="dropdown-menu dropdown-menu-right">
            {Edit_push}
            {Delete_push}
          </div>
        </div>
      );
    }
    return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();

    this.setState({ errormsg: '' });

    this.setState({EdittrainedWork :  record.trainedWork });
    this.setState({Editlocation :  record.locationId });
    this.setState({EdittrainningType :  record.trainningType });
    this.setState({EditdateCompleted :  moment(record.dateCompleted,process.env.API_DATE_FORMAT).format('YYYY-MM-DD')});
    this.setState({EditdateLastWork :  moment(record.dateLastWork,process.env.API_DATE_FORMAT).format('YYYY-MM-DD')});
    this.setState({EditnoOfHoursWork :  record.noOfHoursWork });
    this.setState({trainedLocationId :  record.trainedLocationId });

    this.setState({ isDelete: record.isDelete });
  }

  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById Trained Locations ");
        console.log(data);
        if (data.responseType === "1") {
            
            this.setState({ Addlocation: data.data.locationIdGuid });
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();
              }else{
                this.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      //this.props.history.push("/error-500");
    });
  }

  GetUserTrainedLocations(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    /* Role Management */

    let canDelete = getrole.course_training_can.course_training_can_delete;

    this.showLoader();
    var url=process.env.API_API_URL+'GetUserTrainedLocations?contactId='+this.state.staffContactID+'&canDelete='+canDelete;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserTrainedLocations");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            //this.setState({ ListGrid: data.data.userTrainedLocationView });
            this.setState({ ListGrid: this.rowData(data.data.userTrainedLocationView) })
            this.setState({ trainedWorkList: data.data.trainedWorkList });
            this.setState({ locationList: data.data.trainedLocations });
            
            
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();

              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    //alert(this.state["EdittrainedWork"]);
    if (this.state["EdittrainedWork"] == 0 || this.state["EdittrainedWork"] == "" || this.state["EdittrainedWork"] == null) {
      step1Errors["EdittrainedWork"] = "Please Select Trained/Worked.";
    }

    if (this.state["Editlocation"] == "" || this.state["Editlocation"] == null) {
      step1Errors["Editlocation"] = "Location is mandatory.";
    }

    /*if (this.state["EdittrainningType"] == "" || this.state["EdittrainningType"] == null) {
      step1Errors["EdittrainningType"] = "Training Type is mandatory";
    }*/

    if (this.state["EditdateCompleted"] == ""  || this.state["EditdateCompleted"] == null) {
      step1Errors["EditdateCompleted"] = "Date Completed is mandatory";
    }

    if (this.state["EditdateLastWork"] == "" || this.state["EditdateLastWork"] == null) {
      step1Errors["EditdateLastWork"] = "Date Last Worked is mandatory";
    }

    if (this.state["EditnoOfHoursWork"] == "" || this.state["EditnoOfHoursWork"] == null) {
      step1Errors["EditnoOfHoursWork"] = "of Hrs Worked is mandatory";
    }

    

    console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    var EditdateCompleted=moment(this.state["EditdateCompleted"]).format('MM-DD-YYYY');
    //var NewEditdateCompleted = moment(EditdateCompleted, "MM-DD-YYYY").add(1, 'days');
    
    var EditdateLastWork=moment(this.state["EditdateLastWork"]).format('MM-DD-YYYY');
    //var NewEditdateLastWork = moment(EditdateLastWork, "MM-DD-YYYY").add(1, 'days');

    this.showLoader();

    let ArrayJson = {
          trainedWork: this.state["EdittrainedWork"],
          locationId: this.state["Editlocation"],
          trainningType: this.state["EdittrainningType"],
          dateCompleted: EditdateCompleted,
          dateLastWork: EditdateLastWork,
          noOfHoursWork: this.state["EditnoOfHoursWork"],
          trainedLocationId: this.state["trainedLocationId"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userTrainedLocation"] = ArrayJson;
    bodyarray["userName"] = this.state.staffContactFullname;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'UpdateUserTrainedLocation';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson UpdateUserTrainedLocation");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserTrainedLocations();
            //this.props.history.push('/dashboard');   
        }
        else if (data.responseType === "2") {
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ AddtrainedWork: 0 });
    //this.setState({ Addlocation: 0 });
    this.setState({ AddtrainningType: 0 });
    this.setState({ AdddateCompleted: '' });
    this.setState({ AdddateLastWork: '' });
    this.setState({ AddnoOfHoursWork: '' });

    this.setState({ errormsg: '' });

    //this.GetProfile(); // this.props.primaryLocationId
    this.setState({ Addlocation: localStorage.getItem("primaryLocationGuid") });
    //this.setState({ Addlocation: localStorage.getItem("primaryLocationId") });
    //this.setState({ Addlocation: this.props.primaryLocationId });
  }

  AddRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    //alert(this.state["Addlocation"]);
    
    if (this.state["AddtrainedWork"] == "" || this.state["AddtrainedWork"] == null) {
      step1Errors["AddtrainedWork"] = "Please Select Trained/Worked.";
    }

    if (this.state["Addlocation"] == "" || this.state["Addlocation"] == null) {
      step1Errors["Addlocation"] = "Location is mandatory.";
    }

    /*if (this.state["AddtrainningType"] == "" || this.state["AddtrainningType"] == null) {
      step1Errors["AddtrainningType"] = "Training Type is mandatory";
    }*/

    if (this.state["AdddateCompleted"] == ""  || this.state["AdddateCompleted"] == null) {
      step1Errors["AdddateCompleted"] = "Date Completed is mandatory";
    }

    if (this.state["AdddateLastWork"] == "" || this.state["AdddateLastWork"] == null) {
      step1Errors["AdddateLastWork"] = "Date Last Worked is mandatory";
    }

    if (this.state["AddnoOfHoursWork"] == "" || this.state["AddnoOfHoursWork"] == null) {
      step1Errors["AddnoOfHoursWork"] = "of Hrs Worked is mandatory";
    }

    

    console.log(step1Errors);

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    //return false;
    this.showLoader();

    var AdddateCompleted=moment(this.state["AdddateCompleted"]).format('MM-DD-YYYY');
    //var NewAdddateCompleted = moment(AdddateCompleted, "MM-DD-YYYY").add(1, 'days');
    
    var AdddateLastWork=moment(this.state["AdddateLastWork"]).format('MM-DD-YYYY');
    //var NewAdddateLastWork = moment(AdddateLastWork, "MM-DD-YYYY").add(1, 'days');

    let ArrayJson = {
          trainedWork: this.state["AddtrainedWork"],
          locationId: this.state["Addlocation"],
          trainningType: this.state["AddtrainningType"],
          dateCompleted: AdddateCompleted,
          dateLastWork: AdddateLastWork,
          noOfHoursWork: this.state["AddnoOfHoursWork"]
    };
     
    let bodyarray = {};
    bodyarray["contactId"] = this.state.staffContactID;
    bodyarray["userTrainedLocation"] = ArrayJson;

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'CreateUserTrainedLocation';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson CreateUserTrainedLocation");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {

            this.setState({ AddtrainedWork: 0 });
            this.setState({ Addlocation: 0 });
            this.setState({ AddtrainningType: 0 });
            this.setState({ AdddateCompleted: '' });
            this.setState({ AdddateLastWork: '' });
            this.setState({ AddnoOfHoursWork: '' });

            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".close" ).trigger( "click" );
            this.GetUserTrainedLocations();
            //this.props.history.push('/dashboard');   
        }
        else if (data.responseType === "2") {
            SystemHelpers.ToastError(data.responseMessge);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  DeleteRecord = () => e => {
    e.preventDefault();

    var isdelete = '';
    if(this.state.isDelete== true)
    {
      isdelete = false;
    }
    else
    {
      isdelete = true;
    }

    this.showLoader();
    var url=process.env.API_API_URL+'DeleteUserTrainedLocation?trainedLocationId='+this.state.trainedLocationId+'&isDelete='+isdelete+'&userName='+this.state.staffContactFullname;
    fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson DeleteUserTrainedLocation");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        if (data.responseType === "1") {
            // Profile & Contact
            SystemHelpers.ToastSuccess(data.responseMessge);
            this.GetUserTrainedLocations();
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                this.SessionOut();

              }else{
                SystemHelpers.ToastError(data.message);
              }
              this.hideLoader();
              $( ".cancel-btn" ).trigger( "click" );
        }
        
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  rowData(ListGrid) {
    //console.log(userList)

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    let canDelete = getrole.course_training_can.course_training_can_delete;
    /* Role Management */

    

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        tempdataArray.trainedWork = ListGrid[z].trainedWork;
        tempdataArray.location = ListGrid[z].location;
        if(ListGrid[z].trainningType == 0){
          tempdataArray.trainningType = "";
        }else{
          tempdataArray.trainningType = ListGrid[z].trainningType;
        }
        
        tempdataArray.dateCompleted = moment(ListGrid[z].dateCompleted,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        tempdataArray.dateLastWork = moment(ListGrid[z].dateLastWork,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        tempdataArray.noOfHoursWork = ListGrid[z].noOfHoursWork;

        if(canDelete == true){
          var status = "";
          if(ListGrid[z].isDelete == true){
            tempdataArray.status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            tempdataArray.status = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }
          
        tempdataArray.action = this.Edit_Update_Btn_Func(ListGrid[z]);

        dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }
  render() {
    const data = {
              columns: this.state.header_data,
              rows: this.state.ListGrid
            };
     const { EdittrainedWork, Editlocation, EdittrainningType, EditdateCompleted, EditdateLastWork, EditnoOfHoursWork } = this.props;
      return (
        <div>
        {/* Toast & Loder method use */}
            
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
        <div className="row">
          <div className="col-md-12 d-flex">
            <div className="card profile-box flex-fill">
              <div className="card-body">
                {this.state.role_course_training_can.course_training_can_create == true ?
                  <h3 className="card-title">Trained Locations<a href="#" className="edit-icon" data-toggle="modal" data-target="#EmploymentInformation_trained_locations_Add_modal"><i className="fa fa-plus" /></a></h3>
                  : <h3 className="card-title">Trained Locations <a href="#" className="phss-lock"><i className="fa fa-lock" /></a></h3>
                }
                
                <div className="table-responsive">
                  <MDBDataTable
                    striped
                    bordered
                    small
                    data={data}
                    entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                    className="table table-striped custom-table mb-0 datatable"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* /********** Employment Information Tab Modals *********** */}
        {/* Trained Locations Modal */}
            <div id="EmploymentInformation_trained_locations_Add_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Trained Locations</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()} >
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Trained/Worked<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.AddtrainedWork}  onChange={this.handleChange('AddtrainedWork')}>
                                  <option value=''>-</option>
                                  {this.state.trainedWorkList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddtrainedWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Addlocation} onChange={this.handleChange('Addlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Training Type</label>
                                <select className="form-control" value={this.state.AddtrainningType} onChange={this.handleChange('AddtrainningType')}>
                                  <option value="">-</option>
                                  {this.state.TraininType.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddtrainningType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Completed<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date"  value={this.state.AdddateCompleted} onChange={this.handleChange('AdddateCompleted')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AdddateCompleted"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Last Worked<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date"  value={this.state.AdddateLastWork} onChange={this.handleChange('AdddateLastWork')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["AdddateLastWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label># of Hrs Worked<span className="text-danger">*</span></label>
                                <input type="number" className="form-control" value={this.state.AddnoOfHoursWork} onChange={this.handleChange('AddnoOfHoursWork')} />
                                <span className="form-text error-font-color">{this.state.errormsg["AddnoOfHoursWork"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        {/* //Trained Locations Modal */}
        {/* Trained Locations Edit Modal */}
            <div id="EmploymentInformation_trained_locations_Edit_modal" className="modal custom-modal fade" role="dialog">
              <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content">
                  <div className="modal-header">
                    <h5 className="modal-title">Trained Locations</h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">×</span>
                    </button>
                  </div>
                  <div className="modal-body">
                    <form>
                      <div className="card">
                        <div className="card-body">
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Trained/Worked<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.EdittrainedWork}  onChange={this.handleChange('EdittrainedWork')}>
                                  <option value='0'>-</option>
                                  {this.state.trainedWorkList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.name}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EdittrainedWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Location<span className="text-danger">*</span></label>
                                <select className="form-control" value={this.state.Editlocation} onChange={this.handleChange('Editlocation')}>
                                  <option value="">-</option>
                                  {this.state.locationList.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editlocation"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Training Type</label>
                                <select className="form-control" value={this.state.EdittrainningType} onChange={this.handleChange('EdittrainningType')}>
                                  <option value="">-</option>
                                  {this.state.TraininType.map(( listValue, index ) => {
                                    return (
                                      <option key={index} value={listValue.id}>{listValue.name}</option>
                                    );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EdittrainningType"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Completed<span className="text-danger">*</span></label>
                                  <input className="form-control" type="date"  value={this.state.EditdateCompleted} onChange={this.handleChange('EditdateCompleted')} />
                                  <span className="form-text error-font-color">{this.state.errormsg["EditdateCompleted"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label>Date Last Worked<span className="text-danger">*</span></label>
                                <input className="form-control" type="date"  value={this.state.EditdateLastWork} onChange={this.handleChange('EditdateLastWork')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditdateLastWork"]}</span>
                              </div>
                            </div>
                            <div className="col-md-6">
                              <div className="form-group">
                                <label># of Hrs Worked<span className="text-danger">*</span></label>
                                <input type="number" className="form-control" value={this.state.EditnoOfHoursWork} onChange={this.handleChange('EditnoOfHoursWork')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditnoOfHoursWork"]}</span>
                              </div>
                            </div>
                          </div>
                          <div className="submit-section">
                            <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Update</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
        {/* //Trained Locations Edit Modal */}

        {/* Delete Trained Locations  Modal */}
            <div className="modal custom-modal fade" id="delete_trained_locations" role="dialog">
              <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                  <div className="modal-body">
                    <div className="form-header">
                      <h3>Trained Locations</h3>
                      <p>Are you sure you want to mark trained locations as {this.state.isDelete == true ? 'Active' : 'Inactive' } ?</p>
                    </div>
                    <div className="modal-btn delete-action">
                      <div className="row">
                        <div className="col-6">
                          <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">{this.state.isDelete == true ? 'Active' : 'Inactive' }</a>
                        </div>
                        <div className="col-6">
                          <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        {/* /Delete Trained Locations Modal */}
        {/* /********** Employment Information Tab Modals *********** */}
        </div>
      );
   }
}

export default TrainedLocations;
