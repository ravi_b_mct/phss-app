/**
 * TermsCondition Page
 */
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Reportto,ProfileImage,Avatar_02,Avatar_05,Avatar_09,Avatar_10,Avatar_16,ProfileImageFemale, Applogo} from '../../Entryfile/imagepath'

import moment from 'moment';
import InputMask from 'react-input-mask';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import Loader from '../Loader';
import SystemHelpers from '../Helpers/SystemHelper';

// import other file - Profile Tab
import ProfileView from "./ConfirmProfile/ProfileView";
import ContactView from "./ConfirmProfile/ContactView";
import AddressInfo from "./ConfirmProfile/address";
import EmergencyContactTab from "./ConfirmProfile/EmergencyContact";
// import other file - Profile Tab

export default class ConfirmPersonalProfile extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
        errormsg :  '',
        all_data :  [],
        staffContactID:localStorage.getItem("contactId"),
        //user_role :  {},
        
        /* role_func_call */
        role_func_call : false,
        get_profile_call : false,
        primarylocation: '',
        primaryLocationId:'',
        role_personal_info_can:{},
        role_phss_empinfo_can:{},
        role_emergency_contact_can:{},
        role_employment_history_can:{},
        role_course_training_can:{},
        role_skills_can:{},
        role_consents_waivers_can:{},
        role_awards_recognitions_can:{},
        role_documents_can:{},
        role_human_resource_can:{},
        role_profile_info_can:{},
        role_contact_information_can:{},
        role_address_can :{},
        role_locations_departments_can :{},
        role_volunteer_can :{},

       
           
    };
    this.handleChange = this.handleChange.bind(this);
    this.setPropState = this.setPropState.bind(this);
  }

  setPropState(key, value) {
    this.setState({ [key]: value });
  }

  

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  // toast hide show method
  ToastSuccess(msg){
    toast.success(msg, {
      position: "top-right",
      autoClose: process.env.API_TOAST_TIME,
      hideProgressBar: false,
      closeOnClick: false,
      pauseOnHover: false,
      draggable: false
    });
  }

  ToastError(msg){
    toast.error(msg, {
      position: "top-right",
      autoClose: process.env.API_TOAST_TIME,
      hideProgressBar: false,
      closeOnClick: false,
      pauseOnHover: false,
      draggable: false
    });
  }
  // toast hide show method 

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
  }
  // Input box Type method

  componentDidMount() {

    /* Role Management */
    var pwd = localStorage.getItem("contactId")+"Phss@123";
    var Role_session = localStorage.getItem('sessiontoken');

    var _ciphertext = CryptoAES.decrypt(Role_session, pwd);
    
    //console.log(_ciphertext.toString(CryptoENC));
    var JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));


    console.log('Role Store course_training_can didm staffprofile');

    console.log(JsonCreate);

    this.setState({ role_profile_info_can: JsonCreate.profile_info_can });
    this.setState({ role_contact_information_can: JsonCreate.contact_information_can });
    this.setState({ role_address_can : JsonCreate.address_can });
    this.setState({ role_personal_info_can: JsonCreate.personal_info_can });
    this.setState({ role_phss_empinfo_can: JsonCreate.phss_empinfo_can });
    this.setState({ role_emergency_contact_can: JsonCreate.emergency_contact_can });
    this.setState({ role_employment_history_can: JsonCreate.employment_history_can });
    this.setState({ role_course_training_can: JsonCreate.course_training_can });
    this.setState({ role_skills_can: JsonCreate.skills_can });
    this.setState({ role_consents_waivers_can: JsonCreate.consents_waivers_can });
    this.setState({ role_awards_recognitions_can: JsonCreate.awards_recognitions_can });
    this.setState({ role_documents_can: JsonCreate.documents_can });
    this.setState({ role_human_resource_can: JsonCreate.human_resource_can });
    this.setState({ role_locations_departments_can: JsonCreate.locations_departments_can });
    this.setState({ role_volunteer_can: JsonCreate.volunteer_can });
    
    console.log(JsonCreate);
    console.log(JsonCreate.awards_recognitions_can);
    /* Role Management */
    
    this.GetProfile();
  }

  GetProfile(){
    console.log(localStorage.getItem("token"));
    this.showLoader();
    var url=process.env.API_API_URL+'GetUserBasicInfoById?contactId='+this.state.staffContactID;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      //body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson GetUserBasicInfoById");
        console.log(data);
        //console.log(data.data.userRole);
        // debugger;
        
         
        if (data.responseType === "1") {
            this.setState({ all_data: data.data});
            this.setState({ Preferred_Name: data.data.preferredName});
            this.setState({ get_profile_call: true});
            this.setState({ staffNumberId: data.data.staffNumberId});
            this.setState({ primaryLocationId: data.data.primaryLocationId}); 
            this.setState({ primarylocation: data.data.primaryLocation}); 
            this.setState({ reportToUserName: data.data.reportToUserName});  
            this.setState({ userRoleDisplay: data.data.userRoleDisplay}); 

            this.setState({ userGender: data.data.gender});
            this.setState({ TimeSheetApproverName: data.data.timeSheetApproverName});
            
            localStorage.setItem("primaryLocationId", data.data.primaryLocationId);
            localStorage.setItem("isPayrollAdmin", data.data.isPayrollAdmin);
            
            console.log('primaryLocationId' + data.data.primaryLocationId);

            

            if(data.data.photoAuthorization == null )
            {
              if(data.data.gender == "Female")
              {
                localStorage.setItem('Headerprofileimage', ProfileImageFemale);
              }
              else
              {
                localStorage.setItem('Headerprofileimage', ProfileImage);
              }
              
            }else{
              localStorage.setItem('Headerprofileimage', data.data.photoAuthorization);
            }
            localStorage.setItem('headerisProfileImageShow', data.data.photoRequired);

        }else{
              if(data.message == 'Authorization has been denied for this request.'){
                SystemHelpers.SessionOut();
                this.props.history.push("/login");
              }else{
                SystemHelpers.ToastError(data.message);
              }
              
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }
  
  // Profile update
  // Profile update
  
  render() {

    const { crop, croppedImageUrl, src} = this.state;
    return (
      <div>
          
          {/* Toast & Loder method use */}
          <ToastContainer position="top-right"
              autoClose={process.env.API_TOAST_TIMEOUT}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick={false}
              rtl={false}
              pauseOnVisibilityChange={false}
              draggable={false}
              pauseOnHover={false} />
          {(this.state.loading) ? <Loader /> : null} 
          {/* Toast & Loder method use */}

          <div className="main-wrapper">
            <div className="account-content" >
              
              <div className="container">

                {/* Account Logo */}
                <div className="account-logo pk-account-logo">
                  <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
                </div>
                {/* /Account Logo */}

                {/* /Page Header */}
                <div className="page-header">
                  <div className="row">
                    <div className="col-sm-12">
                      <h3 className="page-title">Confirm Personal Information</h3>
                    </div>
                  </div>
                </div>
                {/* /Page Header */}
                
                <div className="tab-content">
                  
                    {/* //============ Persona Profile ============ */}
                    <div className="row">
                      
                      {this.state.get_profile_call == true && this.state.role_profile_info_can.profile_info_can_view == true ?
                        <ProfileView
                          staffContactID={this.state.staffContactID}
                          profile_info_can={this.state.role_profile_info_can}
                          all_data={this.state.all_data}
                          setPropState={this.setPropState}
                        />
                      :null }

                      {this.state.get_profile_call == true && this.state.role_contact_information_can.contact_information_can_view == true ?
                        <ContactView
                          staffContactID={this.state.staffContactID}
                          contact_information_can={this.state.role_contact_information_can}
                          all_data={this.state.all_data}
                          setPropState={this.setPropState}
                        />
                      :null }

                    </div>
                    {/* //============ Persona Profile ============ */}

                    {/* //============ Address Grid ============ */}

                    {this.state.get_profile_call == true && this.state.role_address_can.address_can_view == true ? 
                      <div>
                          <AddressInfo                          
                            staffContactID={this.state.staffContactID}
                            address_can={this.state.role_address_can}                          
                            setPropState={this.setPropState}
                          />
                      </div>
                      :null
                    }
                                
                    {/* //============ Address Grid ============ */}
                  
                    {/* //=========== Emergency Contacts Tab =========== */}
                    
                    {this.state.get_profile_call == true && this.state.role_emergency_contact_can.emergency_contact_can_view == true ?
                      <EmergencyContactTab 
                        staffContactID={this.state.staffContactID}
                        emergency_contact_can={this.state.role_emergency_contact_can}                  
                        setPropState={this.setPropState}
                      />
                      :<div></div>
                    }
                  
                    {/* //============ Emergency Contacts Tab ============ */}
                </div>
              </div>
              
            </div>
          </div>
          
      </div>
    );
  }
}
