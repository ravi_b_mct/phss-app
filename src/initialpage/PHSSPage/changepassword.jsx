/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';

import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

class Settings extends Component {

  constructor(props) {
    super(props);

    this.state = {
      errormsg : '',
      EditOldPassword : '',
      EditNewPassword : '',
      EditConfirmPassword : ''  
    };
    this.setPropState = this.setPropState.bind(this);
  }

  setPropState(key, value) {
    this.setState({ [key]: value });
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

 handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    if (this.state[input] != '') {
        delete this.state.errormsg[input];
    }
  }

  componentDidMount() {
  }

  UpdateRecord = () => e => {
    //debugger;
    e.preventDefault();

    let step1Errors = {};
    
    if (this.state["EditOldPassword"] =='')
    {
      step1Errors["EditOldPassword"] = "Old Password is mandatory";
    }

    if (this.state["EditNewPassword"] == '' || this.state["EditNewPassword"] == null)
    {
      step1Errors["EditNewPassword"] = "New Password is mandatory";
    }
    else 
    {
      //var regex = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[a-zA-Z!#$%&?@ "])[a-zA-Z0-9!#$%&?@]{8,20}$/
      var regex = /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;
      if (!(regex.test(this.state["EditNewPassword"])))
      {
        step1Errors["EditNewPassword"] = "Password should contain one upper, lower, special char and min 8 char";
      }
    }

    if (this.state["EditConfirmPassword"] == '')
    {
      step1Errors["EditConfirmPassword"] = "Confirm Password is mandatory";
    }
    else if (this.state["EditNewPassword"] !== this.state["EditConfirmPassword"])
    {
      //console.log(this.state["EditNewPassword"]);
      //console.log(this.state["EditConfirmPassword"]);
      step1Errors["EditConfirmPassword"] = "Confirm Password doesn't match"
    }

    

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
      return false;
    }

    //return false;

    this.showLoader();
     
    let bodyarray = {};
    bodyarray["contactId"] = localStorage.getItem("contactId");
    bodyarray["newPass"] = this.state["EditNewPassword"];
    bodyarray["oldPass"] = this.state["EditOldPassword"];

    console.log(bodyarray);
    //return false;
    var url=process.env.API_API_URL+'ChangePasswordDetails';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson ChangePasswordDetails");
        console.log(data);
        //console.log(responseJson);
        // debugger;
        if (data.responseType === "1") {
            //this.props.history.push('/dashboard');
            this.setState({ EditNewPassword: '' });
            this.setState({ EditConfirmPassword: '' });
            this.setState({ EditOldPassword: '' });
            
            SystemHelpers.ToastSuccess(data.responseMessge);
            
                
        }
        else{
            SystemHelpers.ToastError(data.message  );
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
    return false;
  }

  render() {
     
      return (
      <div className="main-wrapper">
      <Header/> 
       <div className="page-wrapper">
       <Helmet>
           <title>Change Password</title>
           <meta name="description" content="Login page"/>					
       </Helmet>
       {/* Page Content */}
       <div className="content container-fluid">
         <div className="row">
           <div className="col-md-8 offset-md-2">
             {/* Page Header */}
             <div className="page-header">
               <div className="row">
                 <div className="col-sm-12">
                   <h3 className="page-title">Change Password</h3>
                 </div>
               </div>
             </div>
             {/* /Page Header */}
             <form>
              <div className="form-group">
                <label>Old password</label>
                <input type="password" className="form-control" value={this.state.EditOldPassword} onChange={this.handleChange('EditOldPassword')} />
                <span className="form-text error-font-color">{this.state.errormsg["EditOldPassword"]}</span>
              </div>
              <div className="form-group">
                <label>New password</label>
                <input type="password" className="form-control" value={this.state.EditNewPassword} onChange={this.handleChange('EditNewPassword')} />
                <span className="form-text error-font-color">{this.state.errormsg["EditNewPassword"]}</span>
                <span class="form-text success-font-color Guidelines">Password should contain a minimum 8 characters with a combination of at least 1 upper case, 1 lower case, 1 number and 1 special character.</span>
              </div>
              <div className="form-group">
                <label>Confirm password</label>
                <input type="password" className="form-control" value={this.state.EditConfirmPassword} onChange={this.handleChange('EditConfirmPassword')} />
                <span className="form-text error-font-color">{this.state.errormsg["EditConfirmPassword"]}</span>
                <span class="form-text success-font-color Guidelines">Password should contain a minimum 8 characters with a combination of at least 1 upper case, 1 lower case, 1 number and 1 special character.</span>
              </div>
              <div className="submit-section">
                <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()} >Update Password</button>
              </div>
            </form>
           </div>
         </div>
       </div>
       {/* /Page Content */}
     </div>
     <SidebarContent/>
      </div>
      );
   }
}

export default Settings;
