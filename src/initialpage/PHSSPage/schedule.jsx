
import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {  Avatar_01 ,Avatar_02,Avatar_03,Avatar_04, Avatar_05, Avatar_08, Avatar_09, Avatar_10,
    Avatar_11,Avatar_12,Avatar_13,Avatar_16   } from "../../Entryfile/imagepath"

import { Table } from 'antd';
import 'antd/dist/antd.css';
import {itemRender,onShowSizeChange} from "./pagination/paginationfunction"
import "../MainPage/antdstyle.css"
import { Multiselect } from 'multiselect-react-dropdown';

import Select from 'react-select-me';
import 'react-select-me/lib/ReactSelectMe.css';

import Header from '../Sidebar/header.jsx';
import SidebarContent from '../Sidebar/sidebar';


import Loader from '../Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from '../Helpers/SystemHelper';

//table
import { MDBDataTable } from 'mdbreact';
import '@fortawesome/fontawesome-free/css/all.min.css';
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbreact/dist/css/mdb.css';
//table

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import PhoneInput from 'react-phone-input-2';

import Datetime from "react-datetime";
import moment from 'moment';

class Schedule extends Component {
  constructor(props) {
    super(props);
    this.state = {
        errormsg :  '',
        
        ListGrid: [],
        ScheduleTypeList : [],
        LocationList : [],
        scheduleId:'',

        staffContactID:localStorage.getItem("contactId"),
        staffContactName:localStorage.getItem("fullName"),

        FilterLocation:localStorage.getItem("primaryLocationGuid"),
        
        // Add
        AddscheduleName : '',
        AddscheduleType : '',
        Addlocation : '',
        AddStartDate:'',
        AddEndDate:'',
        //Addcolor : '',
        // Add

        // Edit
        EditscheduleName : '',
        EditscheduleType : '',
        Editlocation : '',
        EditStartDate:'',
        EditEndDate:'',
        //Editcolor : '',
        // Edit

        role_scheduler_can : {},

        isDelete : false,

        header_data : [],

        TempsearchText:'',

        // css 
        hierarchyId:'',
        page_wrapper_cls:'',
        unauthorized_cls:'',
        // css
    };
    this.setPropState = this.setPropState.bind(this);
    this.handleChange = this.handleChange.bind(this);
       
    this.handleAddStartDate = this.handleAddStartDate.bind(this);
    this.handleAddEndDate = this.handleAddEndDate.bind(this);

    this.handleEditStartDate = this.handleEditStartDate.bind(this);
    this.handleEditEndDate = this.handleEditEndDate.bind(this);
  }

  // Add time Start & End date
  handleAddStartDate = (date) =>{
    console.log('AddStartDate => '+ date);
    this.setState({ AddStartDate : date });
    this.setState({ AddEndDate : '' });
  };
  handleAddEndDate = (date) =>{
    console.log('AddEndDate => '+ date);
    this.setState({ AddEndDate : date });
  };

  validationAddEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.AddStartDate));
  };
  // Add time Start & End date

  // Edit time Start & End date
  handleEditStartDate = (date) =>{
    console.log('EditStartDate => '+ date);
    this.setState({ EditStartDate : date });
    this.setState({ EditEndDate : '' });
  };

  handleEditEndDate = (date) =>{
    console.log('EditEndDate => '+ date);
    this.setState({ EditEndDate : date });
  };

  validationEditEndDate = (currentDate) => {
    return currentDate.isAfter(moment(this.state.EditStartDate));
  };
  // Edit time Start & End date

  setPropState(key, value) {
      this.setState({ [key]: value });
  }

  handleChange = input => e => {
    this.setState({ [input]: e.target.value });
    if (this.state[input] != '') {
      delete this.state.errormsg[input];
    }

    // Add time
    // Add time

    // Edit time
    // Edit time
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method
  
  componentDidMount() {
    /* var timedifference =  MomentTimezone.tz.guess();
    console.log(timedifference);
    alert(timedifference);*/
    /* Role Management */
    console.log('Role Store scheduler_can');
    var getrole = SystemHelpers.GetRole();
    let scheduler_can = getrole.scheduler_can;
    this.setState({ role_scheduler_can: scheduler_can });
    console.log(scheduler_can);
    /* Role Management */

    console.log("GetSchedule");
    console.log(localStorage.getItem("token"));
    this.GetSchedule();
    this.GetLocationList();

    // Check hierarchy
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    console.log("GetSchedule hierarchy = " + hierarchyId);
    this.setState({ hierarchyId: hierarchyId });

    if(hierarchyId == 1){
      this.setState({ page_wrapper_cls: 'page-wrapper pk-page-wrapper-remove hide-div' });
      this.setState({ unauthorized_cls: 'error-box pk-margin-top' });

      setTimeout(() => {
        $('.pk-page-wrapper-remove').remove()
      }, 5000);
      
    }else{
      this.setState({ page_wrapper_cls: 'page-wrapper' });
      this.setState({ unauthorized_cls: 'error-box hide-div' });
    }
    // Check hierarchy

    // Delete Permison
    if(scheduler_can.scheduler_can_delete == true)
    {
      var columns = [
        {
          label: 'Schedule Name',
          field: 'scheduleName',
          sort: 'asc',
          width: 150
        },
        /*{
          label: 'Schedule Location',
          field: 'scheduleLocation',
          sort: 'asc',
          width: 150
        },*/
        {
          label: 'Schedule Type',
          field: 'scheduleType',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Start Date',
          field: 'startDate',
          sort: 'asc',
          width: 150
        },
        {
          label: 'End Date',
          field: 'endDate',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Created on',
          field: 'createdon',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Created by',
          field: 'createdby',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Status',
          field: 'status',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Action',
          field: 'action',
          width: 270
        }
      ];

      this.setState({ header_data: columns });
    }
    else
    {
      var columns = [
        {
          label: 'Schedule Name',
          field: 'scheduleName',
          sort: 'asc',
          width: 150
        },
        /*{
          label: 'Schedule Location',
          field: 'scheduleLocation',
          sort: 'asc',
          width: 150
        },*/
        {
          label: 'Schedule Type',
          field: 'scheduleType',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Start Date',
          field: 'startDate',
          sort: 'asc',
          width: 150
        },
        {
          label: 'End Date',
          field: 'endDate',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Created on',
          field: 'createdon',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Created by',
          field: 'createdby',
          sort: 'asc',
          width: 150
        },
        {
          label: 'Action',
          field: 'action',
          width: 270
        }
      ];

      this.setState({ header_data: columns });
    }
    // Delete Permison
  }

  ClearRecord = ()=> e => {
    e.preventDefault();

    this.setState({ Addlocation: '' });
    this.setState({ AddscheduleType: '' });
    this.setState({ AddscheduleName: '' });

    this.setState({ AddStartDate: '' });
    this.setState({ AddEndDate: '' });

    //$('#input[name="add_color"]').prop('checked', false);
    $('input[type="radio"]').prop('checked', false);
    
    this.setState({ errormsg: '' });
  }

  AddRecord = () => e => {
      e.preventDefault();

      let step1Errors = {};
      
      
      if (this.state["Addlocation"] == '' || this.state["Addlocation"] == null) {
        step1Errors["Addlocation"] = "Schedule Location is mandatory";
      }

      if (this.state["AddscheduleName"] == '' || this.state["AddscheduleName"] == null) {
        step1Errors["AddscheduleName"] = "Schedule Name is mandatory";
      } 
      
      if (this.state["AddscheduleType"] == '' || this.state["AddscheduleType"] == null) {
        step1Errors["AddscheduleType"] = "Schedule Type is mandatory";
      }
      
      if (this.state["AddStartDate"] == '') {
        step1Errors["AddStartDate"] = "Start Date is mandatory";
      }

      if ($("input[name=add_color]:checked")) {
        var Addcolor = $("input[name=add_color]:checked").val();
      } else {
        var Addcolor = "";
      }

      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();

      var AddStartDate=moment(this.state["AddStartDate"]).format('MM-DD-YYYY');

      var add_enddate ='';
      if (this.state["AddEndDate"] != '') {
        var add_enddate=moment(this.state["AddEndDate"]).format('MM-DD-YYYY');
      }

      let ArrayJson = {
        locationId : this.state["Addlocation"],
        ScheduleTypeId : this.state["AddscheduleType"],
        ScheduleName : this.state["AddscheduleName"],
        Color : Addcolor,
        startDate : AddStartDate,
        endDate : add_enddate
      };

      let bodyarray = {};
      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["scheduleDetail"] = ArrayJson;
      
      console.log(bodyarray);
      
      var url=process.env.API_API_URL+'CreateUpdateSceduleDetails';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateSceduleDetails");
          console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);  
            $( ".close" ).trigger( "click" ); 
            this.ClearRecord();
            this.GetSchedule();
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
            SystemHelpers.ToastError(data.responseMessge);  
          } else{
            SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  GetLocationList(){

    /* Role Management */
    var getrole = SystemHelpers.GetRole();
    console.log('Location Get role Schedule');
    console.log(getrole.scheduler_can);
    //let canDelete = getrole.locations_can.locations_can_delete;
    //let locationscanViewall = getrole.locations_can.locations_can_viewall;
    let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
    /* Role Management */
    this.showLoader();
    var url=process.env.API_API_URL+'GetTimeSheetReportView?loggedInUserId='+this.state.staffContactID+"&rolePriorityId="+hierarchyId;
    fetch(url, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("token")
      },
    
    }).then((response) => response.json())
    .then(data => {
      console.log("responseJson GetTimeSheetReportView Schedule");
      console.log(data);

      if (data.responseType === "1") {
        this.setState({ LocationList: data.data.locationViews })
      }else{
        if(data.message == 'Authorization has been denied for this request.'){
          SystemHelpers.SessionOut();
          this.props.history.push("/login");
        }else{
          SystemHelpers.ToastError(data.message);
        }
      }
      this.hideLoader();
    })
    .catch(error => {
      console.log('GetLocationList error Schedule');
      this.props.history.push("/error-500");
    });
  }

  GetSchedule = e => {

      this.setState({ ListGrid: [] })
      
      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      console.log('Schedule Get role');
      console.log(getrole.scheduler_can);
      let canDelete = getrole.scheduler_can.scheduler_can_delete;
      //let canViewall = getrole.scheduler_can.scheduler_can_viewall;
      
      /* Role Management */

      // Check hierarchy
      let hierarchyId = getrole.hierarchy_can.hierarchy_can_id;
      console.log("GetSceduleDetialsLocationwise Entitlement = " + hierarchyId);
      // Check hierarchy

      // User type session decode
      var pwd = localStorage.getItem("contactId")+"Phss@123";
      var UserType_session = localStorage.getItem("usertypesession");

      var _ciphertext = CryptoAES.decrypt(UserType_session, pwd);
      var UserType_JsonCreate = JSON.parse(_ciphertext.toString(CryptoENC));

      console.log('UserType_JsonCreate Schedule');
      console.log(UserType_JsonCreate);
      
      var isCordinator = UserType_JsonCreate.isCordinator;
      // User type session decode
      
      var searchText = this.state.TempsearchText;
      if(searchText!= "" && searchText != null){
        var search_text= '&searchvalue='+searchText;
      }else{
        var search_text= '';
      }

      this.showLoader();
      var url=process.env.API_API_URL+'GetSceduleDetialsLocationwise'+'?rolePriorityId='+hierarchyId+'&contactId='+this.state.staffContactID+'&primaryLocationId='+this.state.FilterLocation+search_text;
      //var url=process.env.API_API_URL+'GetSceduleDetialsLocationwise'+'?isCordinator='+isCordinator+'&contactId='+this.state.staffContactID+'&primaryLocationId='+this.state.FilterLocation+search_text; //localStorage.getItem("primaryLocationId");
      //var url=process.env.API_API_URL+'GetSceduleList'+'?contactId='+this.state.staffContactID+'&canDelete=true'; 
      fetch(url, {
        method: 'GET',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson GetSceduleDetialsLocationwise");
          console.log(data);
          //console.log(data.data.sceduleDetails);
          if (data.responseType === "1") {
              this.setState({ ListGrid: this.rowData(data.data.sceduleDetails) })
              this.setState({ ScheduleTypeList: data.data.sceduleTypeDetails })
              
              this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();    
          }
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  rowData(ListGrid) {
      console.log('row Schedule');
      console.log(this.state.ListGrid);

      /* Role Management */
      var getrole = SystemHelpers.GetRole();
      let canDelete = getrole.scheduler_can.scheduler_can_delete;
      /* Role Management */

      var ListGrid_length = ListGrid.length;
      let dataArray = [];
      var i=1;
      for (var z = 0; z < ListGrid_length; z++) {
        var tempdataArray = [];
        //tempdataArray.rownum = i;
        var color ='';
        if(ListGrid[z].color == "Purple"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-purple pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Orange"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-orange pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Green"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-green pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Pink"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-pink pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Yellow"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-yellow pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Blue"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-blue pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Red"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-red pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Lime"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-lime pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }else if(ListGrid[z].color == "Grey"){
          color = <div> <i class="fa fa-square pk-schedule-label-color-grey pk-schedule-color-code"></i> {ListGrid[z].scheduleName}</div>;
        }

        tempdataArray.scheduleName = color;
        //tempdataArray.scheduleLocation = localStorage.getItem("primaryLocationName");
        tempdataArray.scheduleType = ListGrid[z].sceduleType;

        

        /*if(ListGrid[z].rotation == true){
          tempdataArray.rotation = <div><span className="bg-inverse-success">Yes</span></div>;
        }else{
          tempdataArray.rotation = <div><span className="bg-inverse-warning">No</span></div>;
        }*/
        
        if(ListGrid[z].startDate == ""){
          tempdataArray.startDate = "";
        }else{
          tempdataArray.startDate = moment(ListGrid[z].startDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        }
        
        
        if(ListGrid[z].endDate == ""){
          tempdataArray.endDate = "";
        }else{
          tempdataArray.endDate = moment(ListGrid[z].endDate,process.env.API_DATE_FORMAT).format(process.env.DATE_FORMAT);
        }
        
        //tempdataArray.color = ListGrid[z].color;
        // if(ListGrid[z].color == "Purple"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-purple">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Orange"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-orange">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Green"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-green">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Pink"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-pink">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Yellow"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-yellow">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Blue"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-blue">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Red"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-red">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Lime"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-lime">{ListGrid[z].color}</span></div>;
        // }else if(ListGrid[z].color == "Grey"){
        //   tempdataArray.color = <div><span className="pk-schedule-label-color-grey">{ListGrid[z].color}</span></div>;
        // }
        
        /*if(canDelete == true)
        {
          var status = "";
          if(ListGrid[z].isDelete == true){
            tempdataArray.status = <div><span class="badge bg-inverse-warning">Inactive</span></div>;
          }else{
            tempdataArray.status = <div><span class="badge bg-inverse-success">Active</span></div>;
          }
        }*/
        
        tempdataArray.createdon = moment(ListGrid[z].createdOn).format(process.env.DATETIME_FORMAT);
        tempdataArray.createdby = ListGrid[z].createdBy;

        tempdataArray.status = <div><span class="badge bg-inverse-success">New</span></div>;

        tempdataArray.action = this.Edit_Update_Btn_Func(ListGrid[z]);

        dataArray.push(tempdataArray);
        i++;
      }

      return dataArray;
  }

  Edit_Update_Btn_Func(record){
    let return_push = [];

    if(this.state.role_scheduler_can.scheduler_can_update == true || this.state.role_scheduler_can.scheduler_can_delete == true){
        let Edit_push = [];
        if(this.state.role_scheduler_can.scheduler_can_update == true){
          var url ="/employee-profile/"+record.contactId;
          Edit_push.push(
          <a href={url} onClick={this.EditRecord(record)}  className="dropdown-item" data-toggle="modal" data-target="#edit_schedule"><i className="fa fa-pencil m-r-5" ></i> Edit</a>
          );
        }
        let Delete_push = [];
        if(this.state.role_scheduler_can.scheduler_can_delete == true){

          // if(record.isDelete == false)
          // {
          //   Delete_push.push(
          //     <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-trash-o m-r-5" ></i> Inactive</a>
          //   );
          // }
          // else
          // {
          //   Delete_push.push(
          //     <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-trash-o m-r-5" ></i> Active</a>
          //   );
          // }
          if(record.isDelete == false)
          {
            Delete_push.push(
              <a href="#" onClick={this.EditRecord(record)} className="dropdown-item" data-toggle="modal" data-target="#delete_location"><i className="fa fa-trash-o m-r-5" ></i> Delete</a>
            );
          }
        }
        
        return_push.push(
          <div className="dropdown dropdown-action">
            <a aria-expanded="false" data-toggle="dropdown" className="action-icon dropdown-toggle" href="#"><i className="material-icons">more_vert</i></a>
            <div className="dropdown-menu dropdown-menu-right">
              {Edit_push}
              {Delete_push}
            </div>
          </div>
        );
      }
      return return_push;
  }

  EditRecord = (record) => e => {
    e.preventDefault();
    console.log('Edit Schedule');
    console.log(record);

    this.setState({ Editlocation: record.locationId });
    this.setState({ scheduleId: record.scheduleId });
    this.setState({ EditscheduleType: record.scheduleTypeId });
    this.setState({ EditscheduleName: record.scheduleName });
    
    if(record.rotation == true){
      $('.edit_yes').prop('checked', true);
    }else{
      $('.edit_no').prop('checked', true);
    }

    if(record.color == "Purple"){
      $('.edit_Purple').prop('checked', true);
    }else if(record.color == "Orange"){
      $('.edit_Orange').prop('checked', true);
    }else if(record.color == "Green"){
      $('.edit_Green').prop('checked', true);
    }else if(record.color == "Pink"){
      $('.edit_Pink').prop('checked', true);
    }else if(record.color == "Yellow"){
      $('.edit_Yellow').prop('checked', true);
    }else if(record.color == "Blue"){
      $('.edit_Blue').prop('checked', true);
    }else if(record.color == "Red"){
      $('.edit_Red').prop('checked', true);
    }else if(record.color == "Lime"){
      $('.edit_Lime').prop('checked', true);
    }else if(record.color == "Grey"){
      $('.edit_Grey').prop('checked', true);
    }

    if(record.startDate!="" && record.startDate!=null){
      this.setState({ EditStartDate: moment(record.startDate) });
    }else{
      this.setState({ EditStartDate: "" });
    }
    
    if(record.endDate!="" && record.endDate!=null){
      this.setState({ EditEndDate: moment(record.endDate) });
    }else{
      this.setState({ EditEndDate: "" });
    }
    
    this.setState({ isDelete: record.isDelete });


  }

  UpdateRecord = () => e => {
      e.preventDefault();

      let step1Errors = {};

      if (this.state["Editlocation"] == '' || this.state["Editlocation"] == null) {
        step1Errors["Editlocation"] = "Schedule Location is mandatory";
      }
      
      if (this.state["EditscheduleName"] == '' || this.state["EditscheduleName"] == null) {
        step1Errors["EditscheduleName"] = "Schedule Name is mandatory";
      }
      
      if (this.state["EditscheduleType"] == '' || this.state["EditscheduleType"] == null) {
        step1Errors["EditscheduleType"] = "Schedule Type is mandatory";
      }

      if ($("input[name=edit_color]:checked")) {
        var Editcolor = $("input[name=edit_color]:checked").val();
      }else {
        var Editcolor = "";
      }

      if (this.state["EditStartDate"] == '') {
        step1Errors["EditStartDate"] = "Start Date is mandatory";
      }
      
      console.log(step1Errors);

      this.setState({ errormsg: step1Errors });
          
      if (Object.keys(step1Errors).length > 0) {
        return false;
      }

      this.showLoader();

      var EditStartDate=moment(this.state["EditStartDate"]).format('MM-DD-YYYY');
      
      var edit_end_date ='';
      if (this.state["EditEndDate"] != '') {
        var edit_end_date=moment(this.state["EditEndDate"]).format('MM-DD-YYYY');
      }

      let ArrayJson = {
        locationId : this.state["Editlocation"],
        scheduleId : this.state.scheduleId,
        ScheduleTypeId : this.state["EditscheduleType"],
        ScheduleName : this.state["EditscheduleName"],
        Color : Editcolor,
        StartDate : EditStartDate,
        EndDate : edit_end_date
      };

      let bodyarray = {};

      bodyarray["contactId"] = this.state.staffContactID;
      bodyarray["scheduleDetail"] = ArrayJson;

      console.log(bodyarray);
      
      var url=process.env.API_API_URL+'CreateUpdateSceduleDetails';
      fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
        body: JSON.stringify(bodyarray)
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson CreateUpdateSceduleDetails");
          console.log(data);
          
          if (data.responseType === "1") {
              SystemHelpers.ToastSuccess(data.responseMessge);  
              $( ".close" ).trigger( "click" ); 
              this.GetSchedule();
          }else if (data.responseType === "2" || data.responseType === "3" || data.responseType === "4") {
              SystemHelpers.ToastError(data.responseMessge);  
          }
          else{
              SystemHelpers.ToastError(data.message);
          }
          this.hideLoader();
          
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }

  DeleteRecord = () => e => {
      e.preventDefault();

      var isdelete = '';
      if(this.state.isDelete== true)
      {
        isdelete = false;
      }
      else
      {
        isdelete = true;
      }

      this.showLoader();
      console.log(this.state.scheduleId);
      var url=process.env.API_API_URL+'DeleteSceduleList?scheduleId='+this.state.scheduleId+'&isDelete='+isdelete+'&userName='+localStorage.getItem('fullName');
      fetch(url, {
        method: 'PUT',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
          'Authorization': 'bearer '+localStorage.getItem("token")
        },
      }).then((response) => response.json())
      .then(data => {
          console.log("responseJson DeleteSceduleList");
          console.log(data);
          if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.GetSchedule();
            this.hideLoader();
          }else if (data.responseType == "2" || data.responseType == "3") {
            SystemHelpers.ToastError(data.responseMessge);
            $( ".cancel-btn" ).trigger( "click" );
            this.hideLoader();
          }else{
            if(data.message == 'Authorization has been denied for this request.'){
              SystemHelpers.SessionOut();
              this.props.history.push("/login");
            }else{
              SystemHelpers.ToastError(data.message);
            }
            this.hideLoader();
            $( ".cancel-btn" ).trigger( "click" );
          }
      })
      .catch(error => {
        this.props.history.push("/error-500");
      });
  }


  render() {
      const data = {
        columns: this.state.header_data,
        rows: this.state.ListGrid
      };
                        
    return ( 
      <div className="main-wrapper">
          {/* Toast & Loder method use */}
          {(this.state.loading) ? <Loader /> : null} 
          {/* Toast & Loder method use */}
      <Header/>
        <div className={this.state.unauthorized_cls}>
          {/*<h1>403</h1>*/}
          <h4><i className="fa fa-warning"></i> Sorry, you are not authorized to access this page.</h4>
          {/*<p>Sorry, you are not authorized to access this page.</p>*/}
        </div>
        <div className={this.state.page_wrapper_cls}>
            <Helmet>
                <title>{process.env.WEB_TITLE}</title>
                <meta name="description" content="Login page"/>         
            </Helmet>
              {/* Page Content */}
              <div className="content container-fluid">
                {/* Page Header */}
                <div className="page-header">
                  <div className="row align-items-center">
                    <div className="col">
                      <h3 className="page-title">Schedule</h3>
                      <ul className="breadcrumb">
                        <li className="breadcrumb-item"><a href="/dashboard">Dashboard</a></li>
                        <li className="breadcrumb-item active">Schedule</li>
                      </ul>
                    </div>
                    <div className="col-auto float-right ml-auto">

                      { this.state.role_scheduler_can.scheduler_can_create == true  ?

                        <a href="#" className="btn add-btn" data-toggle="modal" data-target="#add_schedule"><i className="fa fa-plus" /> Add Schedule</a>
                      :<a href="#" className="phss-lock"><i className="fa fa-lock" /></a>
                      }
                    </div>
                  </div>
                </div>
                {/* /Page Header */}

                {/* Search Filter */}
                  <div className="row filter-row">
                    <div className="col-sm-6 col-md-3"> 
                      <div className="form-group  form-focus select-focus">
                        <select className="form-control" id="FilterLocation" value={this.state.FilterLocation}  onChange={this.handleChange('FilterLocation')} >
                          <option value=''>-</option>
                          {this.state.LocationList.map(( listValue, index ) => {
                              return (
                                <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                              );
                          })}
                        </select>
                        <label className="focus-label">Location</label>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2">  
                      <div className="form-group form-focus">
                        <input className="form-control floating" type="text" value={this.state.TempsearchText}  onChange={this.handleChange('TempsearchText')}/>
                        <label className="focus-label">Search</label>
                      </div>
                    </div>
                    <div className="col-sm-6 col-md-2">  
                      <a href="#" className="btn btn-success btn-block" onClick={this.GetSchedule}> Search </a>  
                    </div>
                  </div>
                {/* /Search Filter */}

                <div className="row">
                  <div className="col-md-12">
                    <div className="table-responsive pk-overflow-hide">
                      
                      <MDBDataTable
                        striped
                        bordered
                        small
                        data={data}
                        entriesOptions={[5, 10, 20, 50, 100]} entries={5} pagesAmount={4}
                        className="table table-striped custom-table mb-0 datatable"
                      />
                       
                    </div>
                  </div>
                </div>
              </div>
              {/* /Page Content */}

              {/* Add Schedule Modal */}
              <div id="add_schedule" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Create a Schedule</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Schedule Location<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                {/*<input type="text" className="form-control" value={localStorage.getItem("primaryLocationName")} readonly />*/}
                                <select className="form-control" id="AddReportTo" value={this.state.Addlocation}  onChange={this.handleChange('Addlocation')} >
                                  <option value=''>-</option>
                                  {this.state.LocationList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                      );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Addlocation"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Schedule Name<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                <input type="text" className="form-control" value={this.state.AddscheduleName}  onChange={this.handleChange('AddscheduleName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["AddscheduleName"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Start Date<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.AddStartDate) ? this.state.AddStartDate : ''}
                                  onChange={this.handleAddStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.AddStartDate) ? props.value : ''} />
                                  }}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["AddStartDate"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">End Date<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                <Datetime
                                  isValidDate={this.validationAddEndDate}
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.AddEndDate) ? this.state.AddEndDate : ''}
                                  onChange={this.handleAddEndDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.AddEndDate) ? props.value : ''} />
                                  }}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["AddEndDate"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Schedule Type<span className="text-danger">*</span></label>
                              <div className="col-lg-8"> 
                                <select className="form-control" id="AddReportTo" value={this.state.AddscheduleType}  onChange={this.handleChange('AddscheduleType')} >
                                  <option value=''>-</option>
                                  {this.state.ScheduleTypeList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.scheduleTypeId}>{listValue.sceduleType}</option>
                                      );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["AddscheduleType"]}</span>
                              </div>
                            </div>
                          </div>

                          

                          

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-3 col-form-label">Colour</label>
                              <div className="col-lg-9 pk-schedule-radio-btn">
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Purple" />
                                  <label className="form-check-label" for="color">Purple</label>
                                  <i class="fa fa-square pk-schedule-label-color-purple pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Orange" />
                                  <label className="form-check-label" for="color">Orange</label>
                                  <i class="fa fa-square pk-schedule-label-color-orange pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Green" />
                                  <label className="form-check-label" for="color">Green</label>
                                  <i class="fa fa-square pk-schedule-label-color-green pk-schedule-color-code"></i>
                                </div>
                              </div>

                              <label className="col-lg-3 col-form-label"></label>
                              <div className="col-lg-9 pk-schedule-radio-btn">
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Pink" />
                                  <label className="form-check-label" for="color">Pink</label>
                                  <i class="fa fa-square pk-schedule-label-color-pink pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Yellow" />
                                  <label className="form-check-label" for="color">Yellow</label>
                                  <i class="fa fa-square pk-schedule-label-color-yellow pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Blue" />
                                  <label className="form-check-label" for="color">Blue</label>
                                  <i class="fa fa-square pk-schedule-label-color-blue pk-schedule-color-code"></i>
                                </div>
                              </div>

                              <label className="col-lg-3 col-form-label"></label>
                              <div className="col-lg-9 pk-schedule-radio-btn">
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Red" />
                                  <label className="form-check-label" for="color">Red</label>
                                  <i class="fa fa-square pk-schedule-label-color-red pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Lime" />
                                  <label className="form-check-label" for="color">Lime</label>
                                  <i class="fa fa-square pk-schedule-label-color-lime pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input" type="radio" name="add_color" id="Addcolor" value="Grey" />
                                  <label className="form-check-label" for="color">Grey</label>
                                  <i class="fa fa-square pk-schedule-label-color-grey pk-schedule-color-code"></i>
                                </div>
                              </div>

                            </div>
                          </div>

                          

                          
                          
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn">Cancel</button>
                          <button className="btn btn-primary submit-btn" onClick={this.AddRecord()}>Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Add Schedule Modal */}
              {/* Edit Schedule Modal */}
              <div id="edit_schedule" className="modal custom-modal fade" role="dialog">
                <div className="modal-dialog modal-dialog-centered" role="document">
                  <div className="modal-content">
                    <div className="modal-header">
                      <h5 className="modal-title">Update a Schedule</h5>
                      <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.ClearRecord()}>
                        <span aria-hidden="true">×</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      <form>
                        <div className="row">

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Schedule Location<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                {/*<input type="text" className="form-control" value={localStorage.getItem("primaryLocationName")} readonly />*/}
                                <select className="form-control" id="AddReportTo" value={this.state.Editlocation}  onChange={this.handleChange('Editlocation')} >
                                  <option value=''>-</option>
                                  {this.state.LocationList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.locationGuid}>{listValue.locationName}</option>
                                      );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["Editlocation"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Schedule Name<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                <input type="text" className="form-control" value={this.state.EditscheduleName}  onChange={this.handleChange('EditscheduleName')} />
                                <span className="form-text error-font-color">{this.state.errormsg["EditscheduleName"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Start Date<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditStartDate) ? this.state.EditStartDate : ''}
                                  onChange={this.handleEditStartDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditStartDate) ? props.value : ''} />
                                  }}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditStartDate"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">End Date<span className="text-danger">*</span></label>
                              <div className="col-lg-8">
                                <Datetime
                                  inputProps={{readOnly: true}}
                                  isValidDate={this.validationEditEndDate}
                                  closeOnTab={true}
                                  input={true}
                                  value={(this.state.EditEndDate) ? this.state.EditEndDate : ''}
                                  onChange={this.handleEditEndDate}
                                  dateFormat={process.env.DATE_FORMAT}
                                  timeFormat={false}
                                  renderInput={(props) => {
                                     return <input {...props} value={(this.state.EditEndDate) ? props.value : ''} />
                                  }}
                                />
                                <span className="form-text error-font-color">{this.state.errormsg["EditEndDate"]}</span>
                              </div>
                            </div>
                          </div>

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-4 col-form-label">Schedule Type<span className="text-danger">*</span></label>
                              <div className="col-lg-8"> 
                                <select className="form-control" id="AddReportTo" value={this.state.EditscheduleType}  onChange={this.handleChange('EditscheduleType')} >
                                  <option value=''>-</option>
                                  {this.state.ScheduleTypeList.map(( listValue, index ) => {
                                      return (
                                        <option key={index} value={listValue.scheduleTypeId}>{listValue.sceduleType}</option>
                                      );
                                  })}
                                </select>
                                <span className="form-text error-font-color">{this.state.errormsg["EditscheduleType"]}</span>
                              </div>
                            </div>
                          </div>

                          

                          <div className="col-md-12">
                            <div className="form-group row">
                              <label className="col-lg-3 col-form-label">Colour</label>
                              <div className="col-lg-9 pk-schedule-radio-btn">
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input edit_Purple" type="radio" name="edit_color" id="Editcolor" value="Purple" />
                                  <label className="form-check-label" for="color">Purple</label>
                                  <i class="fa fa-square pk-schedule-label-color-purple pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input edit_Orange" type="radio" name="edit_color" id="Editcolor" value="Orange" />
                                  <label className="form-check-label" for="color">Orange</label>
                                  <i class="fa fa-square pk-schedule-label-color-orange pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input edit_Green" type="radio" name="edit_color" id="Editcolor" value="Green" />
                                  <label className="form-check-label" for="color">Green</label>
                                  <i class="fa fa-square pk-schedule-label-color-green pk-schedule-color-code"></i>
                                </div>
                              </div>

                              <label className="col-lg-3 col-form-label"></label>
                              <div className="col-lg-9 pk-schedule-radio-btn">
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input edit_Pink" type="radio" name="edit_color" id="Editcolor" value="Pink" />
                                  <label className="form-check-label" for="color">Pink</label>
                                  <i class="fa fa-square pk-schedule-label-color-pink pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input edit_Yellow" type="radio" name="edit_color" id="Editcolor" value="Yellow" />
                                  <label className="form-check-label" for="color">Yellow</label>
                                  <i class="fa fa-square pk-schedule-label-color-yellow pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input edit_Blue" type="radio" name="edit_color" id="Editcolor" value="Blue" />
                                  <label className="form-check-label" for="color">Blue</label>
                                  <i class="fa fa-square pk-schedule-label-color-blue pk-schedule-color-code"></i>
                                </div>
                              </div>

                              <label className="col-lg-4 col-form-label"></label>
                              <div className="col-lg-8 pk-schedule-radio-btn">
                                <div className="form-check form-check-inline  col-md-3">
                                  <input className="form-check-input edit_Red" type="radio" name="edit_color" id="Editcolor" value="Red" />
                                  <label className="form-check-label" for="color">Red</label>
                                  <i class="fa fa-square pk-schedule-label-color-red pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input edit_Lime" type="radio" name="edit_color" id="Editcolor" value="Lime" />
                                  <label className="form-check-label" for="color">Lime</label>
                                  <i class="fa fa-square pk-schedule-label-color-lime pk-schedule-color-code"></i>
                                </div>
                                <div className="form-check form-check-inline col-md-3">
                                  <input className="form-check-input edit_Grey" type="radio" name="edit_color" id="Editcolor" value="Grey" />
                                  <label className="form-check-label" for="color">Grey</label>
                                  <i class="fa fa-square pk-schedule-label-color-grey pk-schedule-color-code"></i>
                                </div>
                              </div>

                            </div>
                          </div>

                          

                          
                          
                        </div>
                        <div className="submit-section">
                          <button className="btn btn-primary submit-btn">Cancel</button>
                          <button className="btn btn-primary submit-btn" onClick={this.UpdateRecord()}>Submit</button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              {/* /Edit Schedule Modal */}
              {/* Delete Schedule  Modal */}
                <div className="modal custom-modal fade" id="delete_location" role="dialog">
                  <div className="modal-dialog modal-dialog-centered">
                    <div className="modal-content">
                      <div className="modal-body">
                        <div className="form-header">
                          <h3>Schedule</h3>
                          <p>Are you sure you want to mark Schedule as Delete ?</p>
                        </div>
                        <div className="modal-btn delete-action">
                          <div className="row">
                            <div className="col-6">
                              <a  onClick={this.DeleteRecord()} className="btn btn-primary continue-btn">Delete</a>
                            </div>
                            <div className="col-6">
                              <a href="" data-dismiss="modal" className="btn btn-primary cancel-btn">Cancel</a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              {/* /Delete Schedule Modal */}
            </div>
          <SidebarContent/>
      </div>
        );
      
   }
}

export default Schedule;
