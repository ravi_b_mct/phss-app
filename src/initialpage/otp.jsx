/**
 * Signin Firebase
 */

import React, { Component } from 'react';
import { Helmet } from "react-helmet";
import {Applogo} from '../Entryfile/imagepath.jsx';

import Loader from './Loader';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import SystemHelpers from './Helpers/SystemHelper';

import CryptoAES from 'crypto-js/aes';
import CryptoENC from 'crypto-js/enc-utf8';

import uniqueString from 'unique-string';

class OTPscreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
        num1 : '',
        num2 : '',
        num3 : '',
        num4 : '',
        otp : '',
        errormsg :  '',
        machineId: ''
    };
    this.handleChange = this.handleChange.bind(this)
  }

  // Loader hide show method
  hideLoader = () => {
    this.setState({ loading: false });
  }

  showLoader = () => {
    this.setState({ loading: true });
  }
  // Loader hide show method

  

  // Input box Type method
  handleChange = input => e => {
    this.setState({ [input]: e.target.value });

    const { maxLength, value, name } = e.target;
    const [fieldName, fieldIndex] = name.split("-");

    // Check if they hit the max character length
    if (value.length >= maxLength) {
      // Check if it's not the last input field
      if (parseInt(fieldIndex, 10) < 4) {
        // Get the next input field
        const nextSibling = document.querySelector(
          `input[name=ssn-${parseInt(fieldIndex, 10) + 1}]`
        );

        // If found, focus the next field
        if (nextSibling !== null) {
          nextSibling.focus();
        }
      }
    }
  }
  componentDidMount() {

    const { data } = this.props.location;
    if (localStorage.getItem("temp_login_token") == null) {
        this.props.history.push("/login");
    }

    if (location.pathname.includes("login") || location.pathname.includes("register") || location.pathname.includes("forgotpassword")
      || location.pathname.includes("resetpassword") || location.pathname.includes("mobilesubmit") || location.pathname.includes("otp")|| location.pathname.includes("lockscreen") ) {
          $('body').addClass('account-page');
      }else if (location.pathname.includes("error-404") || location.pathname.includes("error-500") ) {
          $('body').addClass('error-page');
      }

    // $(".otp-input").keyup(function () {
    //     if (this.value.length == this.maxLength) {
    //       $(this).next('.otp-input').focus();
    //     }
    // });

    if (localStorage.getItem('machineId') != null)
    {
      var local_machine_id = localStorage.getItem('machineId');
      var pwd = "Phss@123";
      var decrypt_machineId = CryptoAES.decrypt(local_machine_id, pwd);
      this.setState({ machineId: decrypt_machineId.toString(CryptoENC) });
    }

    
  }

  VerifyLoginOTP = () => e => {
    var otp =this.state["num1"]+this.state["num2"]+this.state["num3"]+this.state["num4"];
    

    let step1Errors = {};
    if (otp.length !=4) {
      step1Errors["otp"] = "Please Enter Valid OTP";
    }

    this.setState({ errormsg: step1Errors });
        
    if (Object.keys(step1Errors).length > 0) {
        return false;
    }

    this.showLoader();
     
    let bodyarray = {};
    bodyarray["UserName"] = localStorage.getItem("temp_login_UserName");
    bodyarray["Password"] = localStorage.getItem("temp_login_Password");
    bodyarray["ContactId"] = localStorage.getItem("temp_login_id");
    bodyarray["ipAddress"] = this.state["machineId"];
    bodyarray["deviceToken"] = this.state["machineId"];
    bodyarray["Otp"] = otp;

    var url=process.env.API_API_URL+'VerifyLoginOTP';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("temp_login_token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson VerifyLoginOTP");
        console.log(data);
        //console.log(data.data.userRole);
        //console.log(JSON.parse(JSON.stringify(data.data.userRole)));
        // debugger;
        if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);

            localStorage.setItem("token", localStorage.getItem("temp_login_token"));
            localStorage.removeItem('temp_login_token');
            localStorage.removeItem('temp_login_id');
            localStorage.removeItem('temp_login_UserName');
            localStorage.removeItem('temp_login_Password');

            //localStorage.setItem("token", data.token);
            localStorage.setItem("contactId", data.data.contactId);
            localStorage.setItem("eMailAddress", data.data.eMailAddress);
            localStorage.setItem("fullName", data.data.fullName);
            localStorage.setItem("userRole", data.data.userRole);

            this.props.history.push('/dashboard');
        }
        else if (data.responseType === "2") {
          SystemHelpers.ToastError(data.responseMessge);
        }else{
          SystemHelpers.ToastError(data.message);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  ResendLoginOTP = () => e => {
    
    this.showLoader();
     
    let bodyarray = {};
    bodyarray["ContactId"] = localStorage.getItem("temp_login_id");
    bodyarray["otpFor"] = "login";

    var url=process.env.API_API_URL+'ResendOtP';
    fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'bearer '+localStorage.getItem("temp_login_token")
      },
      body: JSON.stringify(bodyarray)
    }).then((response) => response.json())
    .then(data => {
        console.log("responseJson temp_login_token");
        console.log(data);
        //console.log(data.data.userRole);
        //console.log(JSON.parse(JSON.stringify(data.data.userRole)));
        // debugger;
        if (data.responseType === "1") {
            SystemHelpers.ToastSuccess(data.responseMessge);

            

            this.props.history.push('/OTPVerification');
        }
        else if (data.responseType === "2") {
          SystemHelpers.ToastError(data.responseMessge);
        }else{
          SystemHelpers.ToastError(data.message);
        }
        this.hideLoader();
        
    })
    .catch(error => {
      this.props.history.push("/error-500");
    });
  }

  render() {
     const { num1, num2, num3, num4 } = this.props;
      return ( 
        <div>
        {/* Toast & Loder method use */}
        <ToastContainer position="top-right"
            autoClose={process.env.API_TOAST_TIMEOUT}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick={false}
            rtl={false}
            pauseOnVisibilityChange={false}
            draggable={false}
            pauseOnHover={false} />
        {(this.state.loading) ? <Loader /> : null} 
        {/* Toast & Loder method use */}
      <div className="main-wrapper">
          <Helmet>
               <title>OTP - PARTICIPATION HOUSE SUPPORT SERVICES</title>
               <meta name="description" content="Login page"/>					
         </Helmet>
      <div className="account-content">
        <div className="container">
          {/* Account Logo */}
          <div className="account-logo">
            <a href="/login"><img src={Applogo} className="pk-logo" alt="PARTICIPATION HOUSE SUPPORT SERVICES" /></a>
          </div>
          {/* /Account Logo */}
          <div className="account-box">
            <div className="account-wrapper">
              <h3 className="account-title">OTP</h3>
              <p className="account-subtitle">Please enter the 4 digit code sent to your email address</p>
              {/* Account Form */}
              
                <div className="otp-wrap">
                  <input type="text" name="ssn-1" placeholder={0} maxLength={1} className="otp-input" value={num1} onChange={this.handleChange('num1')}/>
                  <input type="text" name="ssn-2" placeholder={0} maxLength={1} className="otp-input" value={num2} onChange={this.handleChange('num2')}/>
                  <input type="text" name="ssn-3" placeholder={0} maxLength={1} className="otp-input" value={num3} onChange={this.handleChange('num3')}/>
                  <input type="text" name="ssn-4" placeholder={0} maxLength={1} className="otp-input" value={num4} onChange={this.handleChange('num4')}/>
                  <span className="form-text error-font-color">{this.state.errormsg["otp"]}</span>
                </div>
                
                <div className="form-group text-center">
                  <button className="btn btn-primary account-btn" onClick={this.VerifyLoginOTP()}>Enter</button>
                </div>
                <div className="account-footer">
                  <p>Not yet received? <a onClick={this.ResendLoginOTP()} >Resend OTP</a></p>
                  {/*<p>Not yet received? <button className="" onClick={this.ResendLoginOTP()}>Resend OTP</button></p>*/}
                </div>
              
              {/* /Account Form */}
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>     
      );
   }
}

export default OTPscreen;
